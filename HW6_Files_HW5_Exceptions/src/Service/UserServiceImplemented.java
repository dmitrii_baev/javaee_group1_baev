package Service;

import Exceptions.BadEmailException;
import Exceptions.BadPasswordException;
import Exceptions.UserAlreadyExistsException;
import Exceptions.UserNotFoundException;
import Repository.UsersRepository;

import static Repository.UsersRepositoryList.DEFAULT_SIZE_MULTIPLICATOR;
import static Repository.UsersRepositoryList.DEFAULT_USER_ARRAY_SIZE;

public class UserServiceImplemented implements UsersService{
    private User[] usersLocalRepository;
    private int length;
    private int currentIndex;
    private UsersRepository currentRepository;

    public UserServiceImplemented(UsersRepository currentRepository) {
        this.usersLocalRepository = new User[DEFAULT_USER_ARRAY_SIZE];
        this.length = DEFAULT_USER_ARRAY_SIZE;
        this.currentRepository = currentRepository;
        this.currentIndex = 0;
    }

    public UserServiceImplemented() {
        this.usersLocalRepository = new User[DEFAULT_USER_ARRAY_SIZE];
        this.length = DEFAULT_USER_ARRAY_SIZE;
        this.currentRepository = null;
        this.currentIndex = 0;
    }

    public void addUser (User user){
        checkSize();
        if (this.getUser(user.getEmail()) == null) {
            usersLocalRepository[currentIndex] = user;
            currentIndex++;
        } else throw new UserAlreadyExistsException("User with this email already exists");
    }

    public User getUser(String name) {
        if (name == null) return null;
        User tempUser = new User(name, User.DEFAULT_USER_PASSWORD);
        for (int i = 0; i < currentIndex; i++){
            if (usersLocalRepository[i].equals(tempUser)) return usersLocalRepository[i];
        }
        return null;
    }

    @Override
    public void signUp(String email, String password) {

        //check email
        char emailSybols[] = email.toCharArray();
        boolean arobaseInEmail = false;
        for (int i = 0; i < emailSybols.length; i++){
            if (emailSybols[i] == '@')  {
                arobaseInEmail = true;
                break;
            }
        }
        if (!arobaseInEmail) throw new BadEmailException("there should be symbol '@' in an email");

        //check password
        String passwordSybols[] = password.split("");
        boolean digitsInPassword = false;
        boolean lettersInPassword = false;
        String digits = "1234567890";
        String letters = "qwertyuiopasdfghjklzxcvbnm";
        if (passwordSybols.length < 8) throw new BadPasswordException("password should be longer then 7 symbols");

        for (int i = 0; i < passwordSybols.length; i++){
            if (digitsInPassword && lettersInPassword) break;
            if (digits.contains(passwordSybols[i]))  digitsInPassword = true;
            else {if (letters.contains(passwordSybols[i])) lettersInPassword = true;}
        }
        if (!digitsInPassword) throw new BadPasswordException("there should be numbers in password");
        if (!lettersInPassword) throw new BadPasswordException("there should be letters in password");

        //singing up user
        User userAdded = new User(email, password);
        this.addUser(userAdded);
        System.out.println("You were successfully signed up");
    }

    //public void signUp(String email, String password, UsersRepository repository) {}

    @Override
    public void signIn(String email, String password) {
        User tempUser = new User(email, password);
        if (this.getUser(email) != null && this.getUser(email).getEmail() == email
                && this.getUser(email).getPassword() == password) {
            System.out.println("You have signed in");
        } else {throw new UserNotFoundException("Authentification error");}
    }

    //service methods
    public void checkSize(){
        if (currentIndex >= length){
            User[] temp = copyOfData();
            length *= DEFAULT_SIZE_MULTIPLICATOR;
            usersLocalRepository = cloneThisArray(temp, 0, 0, length);
        }
    }
    public User[] copyOfData(){
        User copyOfArray[] = new User[this.length];
        for (int i = 0; i < this.length; i++) copyOfArray[i] = usersLocalRepository[i];
        return copyOfArray;
    }

    public User[] cloneThisArray(User[] originalArray, int startindexOfOriginal, int startIndexOfClone, int cloneLength) {
        User[] cloneOfArray = (User[]) new Object[cloneLength];
        if (cloneOfArray.length >= (originalArray.length - startIndexOfClone)) {
            for (int i = startindexOfOriginal; i < (originalArray.length - startindexOfOriginal); i++) {
                cloneOfArray[startIndexOfClone + i - startindexOfOriginal] = originalArray[i];
            }
            return cloneOfArray;
        } else {
            System.out.println("несовпадение размеров массивов");
            return null;
        }
    }
}
