package Service;

import java.util.Objects;

public class User {
    public static final String DEFAULT_USER_PASSWORD = "1234qwer";

    private String email;
    private String password;
    private Integer id;

    public User(String email, String password, Integer id) {
        this.email = email;
        this.password = password;
        this.id = id;
    }

    public User(Integer id, String email, String password) {
        this.email = email;
        this.password = password;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
        this.id = null;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email) && Objects.equals(password, user.password) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }

    @Override
    public String toString() {
        return id + "|" + email + "|" + password;
    }
}
