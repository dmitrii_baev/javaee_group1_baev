package Service;

import Exceptions.BadEmailException;
import Exceptions.BadPasswordException;
import Exceptions.UserAlreadyExistsException;
import Exceptions.UserNotFoundException;
import Repository.UsersRepository;

import java.util.function.Function;

import static Repository.UsersRepositoryList.DEFAULT_SIZE_MULTIPLICATOR;
import static Repository.UsersRepositoryList.DEFAULT_USER_ARRAY_SIZE;

public class UserServiceFileImplemented implements UsersService{

    private UsersRepository currentRepository;

    public UserServiceFileImplemented(UsersRepository currentRepository) {
        this.currentRepository = currentRepository;
    }

    public void addUser (User user){
        currentRepository.update(user);
    }

    public User getUser(String email, String password) {
        return currentRepository.getUser(email, password);
    }

    @Override
    public void signUp(String email, String password) {

        //check email
        char emailSybols[] = email.toCharArray();
        boolean arobaseInEmail = false;
        for (int i = 0; i < emailSybols.length; i++){
            if (emailSybols[i] == '@')  {
                arobaseInEmail = true;
                break;
            }
        }
        if (!arobaseInEmail) throw new BadEmailException("there should be symbol '@' in an email");

        //check password
        String passwordSybols[] = password.split("");
        boolean digitsInPassword = false;
        boolean lettersInPassword = false;
        String digits = "1234567890";
        String letters = "qwertyuiopasdfghjklzxcvbnm";
        if (passwordSybols.length < 8) throw new BadPasswordException("password should be longer then 7 symbols");

        for (int i = 0; i < passwordSybols.length; i++){
            if (digitsInPassword && lettersInPassword) break;
            if (digits.contains(passwordSybols[i]))  digitsInPassword = true;
            else {if (letters.contains(passwordSybols[i])) lettersInPassword = true;}
        }
        if (!digitsInPassword) throw new BadPasswordException("there should be numbers in password");
        if (!lettersInPassword) throw new BadPasswordException("there should be letters in password");

        //singing up user
        User userAdded = new User(email, password);
        this.addUser(userAdded);
        System.out.println("You were successfully signed up");
    }

    //public void signUp(String email, String password, UsersRepository repository) {}

    @Override
    public void signIn(String email, String password) {
        User tempUser = new User(email, password);
        if (this.getUser(email, password) != null && this.getUser(email, password).getEmail() == email
                && this.getUser(email, password).getPassword() == password) {
            System.out.println("You have signed in");
        } else {throw new UserNotFoundException("Authentification error");}
    }

    //service methods

}
