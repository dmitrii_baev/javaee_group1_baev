import Repository.IdGeneratorFileImpl;
import Repository.UsersRepositoryFileImpl;
import Service.User;
import Service.UserServiceFileImplemented;
import Service.UserServiceImplemented;

public class Main_Files {
    public static void main(String[] args) {
        IdGeneratorFileImpl idGenerator = new IdGeneratorFileImpl("C:\\JAVA\\javaee_group1_baev_prepared\\HW6_Files_HW5_Exceptions\\src\\users_id.txt");
        UsersRepositoryFileImpl usersFiles = new UsersRepositoryFileImpl("C:\\JAVA\\javaee_group1_baev_prepared\\HW6_Files_HW5_Exceptions\\src\\users.txt", idGenerator);
        UserServiceFileImplemented newService = new UserServiceFileImplemented(usersFiles);
        User user = new User("ya@", "111f111111");
        usersFiles.update(user);
        User user2 = new User("ya@ne", "aefe454sgr");
        usersFiles.update(user2);
        User user3 = new User("ya@", "122f111111");
        usersFiles.update(user3);
//        newService.signUp("ya@", "111f111111");
//        newService.signIn("ya@", "111f111111");
//        newService.signIn("ya", "123sghdkvj");
    }
}
