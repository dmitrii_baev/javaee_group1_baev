package Repository;

import Service.User;

public interface UsersRepository {
    public static final int DEFAULT_SIZE_MULTIPLICATOR = 2;
    public static final int DEFAULT_USER_ARRAY_SIZE = 10;
    public User getUser(String name, String password);
    public void addUser(User user);
    public void save(User user);
    public void update(User user);
}
