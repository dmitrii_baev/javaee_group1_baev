package Repository;

import Exceptions.BadFileException;
import Exceptions.UserNotFoundException;
import Service.User;

import java.io.*;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.List;


public class UsersRepositoryFileImpl implements UsersRepository{

    private String fileName;
    private final IdGenerator idGenerator;

    public UsersRepositoryFileImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private static final Function<String, User> userMapper = line -> {
        String[] parsedLine = line.split("\\|");
        return new User(parsedLine[1],
                parsedLine[2],
                Integer.parseInt(parsedLine[0]));
    };

    @Override
    public User getUser(String name, String password){
        try (Scanner scanner = new Scanner(new FileInputStream(fileName))) {
            User tempUser = new User(name, password);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] userLine = line.split("\\|");
                if (userLine.length == 3) {
                    User returnableUser = new User(userLine[1], userLine[2], Integer.parseInt(userLine[0]));
                    if (returnableUser.equals(tempUser)) return returnableUser;
                } //else throw new BadFileException("wrong string in users.txt");
            }
            scanner.close();
            return null;
        } catch (BadFileException f) {
            throw new IllegalArgumentException(f);
        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void addUser (User user){
        // В этом методе нет смысла, оставил его тут только для того, чтобы не менять интерфейс из 5го задания
        // Функционал этого метода заменен методом save и update
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            user.setId(idGenerator.next());
            writer.write(user.toString());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            user.setId(idGenerator.next());
            writer.write(user.toString());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User user) {
        User updatedUser = this.getUser(user.getEmail(), user.getPassword());
        if (updatedUser == null){
            this.save(user);
        } else {
//            try (Scanner sc = new Scanner(new File(fileName));){
//                StringBuffer buffer = new StringBuffer();
//                while (sc.hasNextLine()) {
//                    buffer.append(sc.nextLine()+System.lineSeparator());
//                }
//                String fileContents = buffer.toString();
//                System.out.println("Contents of the file: "+fileContents);
//                String oldLine = updatedUser.toString();
//                String newLine = user.toString();
//                fileContents = fileContents.replaceAll(oldLine, newLine);
//                sc.close();
//                try (FileWriter writer = new FileWriter(fileName)){
//                    writer.append(fileContents);
//                    writer.flush();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName));){
                List<User> users = reader
                        .lines()
                        .map(userMapper)
                        .collect(Collectors.toList());
                reader.close();
                for (int i = 0; i < users.size(); i++){
                    if (users.get(i).getEmail().equals(user.getEmail())){
//                        users.get(i).setId(user.getId());
                        users.get(i).setPassword(user.getPassword());
                    }
                }
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false));) {
                    for (int j = 0; j < users.toArray().length; j++) {
                        User newUser = users.get(j);
                        writer.write(newUser.toString());
                        writer.newLine();
                    }
                }
                catch (FileNotFoundException e) {
                    throw new IllegalArgumentException(e);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
