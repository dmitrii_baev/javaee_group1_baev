package Repository;

import Exceptions.UserNotFoundException;
import Service.User;


public class UsersRepositoryList implements UsersRepository{

    private User[] usersLocalRepository;
    private int length;
    private int currentIndex;

    public UsersRepositoryList() {
        this.length = DEFAULT_USER_ARRAY_SIZE;
        this.usersLocalRepository = new User[DEFAULT_USER_ARRAY_SIZE];
        this.currentIndex = 0;
    }

    @Override
    public User getUser(String name, String password) {
        User tempUser = new User(name, password);
        for (int i = 0; i < currentIndex; i++){
            if (usersLocalRepository[i].equals(tempUser)) return usersLocalRepository[i];
        }
        return null;
    }

    @Override
    public void save(User user){
        checkSize();
        if (this.getUser(user.getEmail(), user.getPassword()) == null) {
            usersLocalRepository[currentIndex] = user;
            currentIndex++;
        } else throw new UserNotFoundException("User with this email already exists");
    }

    @Override
    public void update(User user){
    }

    @Override
    public void addUser(User user){
        checkSize();
        if (this.getUser(user.getEmail(), user.getPassword()) == null) {
            usersLocalRepository[currentIndex] = user;
            currentIndex++;
        } else throw new UserNotFoundException("User with this email already exists");
    }



    //service methods
    public void checkSize(){
        if (currentIndex >= length){
            User[] temp = copyOfData();
            length *= DEFAULT_SIZE_MULTIPLICATOR;
            usersLocalRepository = cloneThisArray(temp, 0, 0, length);
        }
    }
    public User[] copyOfData(){
        User copyOfArray[] = new User[this.length];
        for (int i = 0; i < this.length; i++) copyOfArray[i] = usersLocalRepository[i];
        return copyOfArray;
    }

    public User[] cloneThisArray(User[] originalArray, int startindexOfOriginal, int startIndexOfClone, int cloneLength) {
        User[] cloneOfArray = (User[]) new Object[cloneLength];
        if (cloneOfArray.length >= (originalArray.length - startIndexOfClone)) {
            for (int i = startindexOfOriginal; i < (originalArray.length - startindexOfOriginal); i++) {
                cloneOfArray[startIndexOfClone + i - startindexOfOriginal] = originalArray[i];
            }
            return cloneOfArray;
        } else {
            System.out.println("несовпадение размеров массивов");
            return null;
        }
    }
}
