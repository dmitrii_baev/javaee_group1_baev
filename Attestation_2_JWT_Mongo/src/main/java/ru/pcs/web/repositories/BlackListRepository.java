package ru.pcs.web.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import ru.pcs.web.models.Token;

import java.util.List;

public interface BlackListRepository extends MongoRepository<Token, String> {
    Token findTokenByToken(String token);
}
