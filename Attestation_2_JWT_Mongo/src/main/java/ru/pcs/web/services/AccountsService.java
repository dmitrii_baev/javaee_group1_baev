package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.models.Account;

import java.util.List;
import java.util.Optional;

public interface AccountsService {
    AccountDto addAccount(AccountDto account);
    List<AccountDto> getAccounts(int page, int size);
    Optional<Account> getAccountByEmail(String email);

}
