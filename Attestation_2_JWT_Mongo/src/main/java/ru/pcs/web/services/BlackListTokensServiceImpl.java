package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.models.Token;
import ru.pcs.web.repositories.BlackListRepository;

import java.util.List;


@Service
@RequiredArgsConstructor
public class BlackListTokensServiceImpl implements BlackListTokensService {

    private final BlackListRepository blackListRepository;

    @Override
    public void addToken(String token) {
        Token currentToken = Token.builder()
                .token(token)
                .build();
        if (!findToken(token)) {
            blackListRepository.save(currentToken);
        }
    }

    @Override
    public boolean findToken(String token) {
        return blackListRepository.findTokenByToken(token) != null;
    }
}
