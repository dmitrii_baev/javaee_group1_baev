package ru.pcs.web.services;

public interface BlackListTokensService {
    void addToken(String token);

    boolean findToken(String token);
}
