package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.models.Account;
import ru.pcs.web.repositories.AccountsRepository;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;

    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Override
    public AccountDto addAccount(AccountDto account) {
        Account newAccount = Account.builder()
                .email(account.getEmail())
                .password(passwordEncoder.encode(account.getPassword()))
                .role(Account.Role.USER)
                .build();

        accountsRepository.save(newAccount);

        return AccountDto.from(newAccount);
    }

    @Override
    public List<AccountDto> getAccounts(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Account> result = accountsRepository.findAll(request);
        return AccountDto.from(result.getContent());
    }

    @Override
    public Optional<Account> getAccountByEmail(String email) {
        return accountsRepository.findByEmail(email);
    }

}
