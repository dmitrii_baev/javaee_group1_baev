package Service;

import java.lang.reflect.Field;

public class Validator {

    public Validator() {
    }

    public void validate(Object form) {
        Class<Object> documentClass = (Class<Object>) form.getClass();
        Field[] fields = documentClass.getDeclaredFields();

        for (Field field : fields) {
            Min minValue = field.getAnnotation(Min.class);
            Max maxValue = field.getAnnotation(Max.class);
            MinMaxLength lengthLimitations = field.getAnnotation(MinMaxLength.class);
            NotEmpty isItEmpty = field.getAnnotation(NotEmpty.class);
            if (isItEmpty != null && field.getType() == String.class) {
                try {
                    field.setAccessible(true);
                    if (field.get(form) == "")
                        throw new IllegalArgumentException("The value of field " + field + " in " + form.getClass() + " " + form + " is empty");
                    field.setAccessible(true);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
            if (lengthLimitations != null && field.getType() == String.class) {
                try {
                    field.setAccessible(true);
                    String fieldString = (String) field.get(form);
                    if (fieldString.length() < lengthLimitations.min())
                        throw new IllegalArgumentException("The value of field " + field + " in " + form.getClass() + " " + form + " is less then @MinMaxLength " + lengthLimitations.min());
                    else if (fieldString.length() > lengthLimitations.max())
                        throw new IllegalArgumentException("The value of field " + field + " in " + form.getClass() + " " + form + " is more then @MinMaxLength " + lengthLimitations.max());
                    field.setAccessible(true);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
            if (minValue != null && field.getType() == int.class) {
                try {
                    field.setAccessible(true);
                    int valueMinimum = minValue.value();
                    if (valueMinimum > field.getInt(form))
                        throw new IllegalArgumentException("The value of field " + field + " in " + form.getClass() + " " + form + " less then @Min " + valueMinimum);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
            if (maxValue != null && field.getType() == int.class) {
                try {
                    field.setAccessible(true);
                    int valueMaximum = maxValue.value();
                    if (valueMaximum < field.getInt(form))
                        throw new IllegalArgumentException("The value of field " + field + " in " + form.getClass() + " " + form + " more then @Max " + valueMaximum);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Validator{}";
    }
}
