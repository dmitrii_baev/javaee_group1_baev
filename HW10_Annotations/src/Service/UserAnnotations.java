package Service;

import java.util.Objects;

public class UserAnnotations {
    //@NotEmpty
    @NotEmpty
    @MinMaxLength(min = 2, max = 30)
    private String name;
    @Max(150)
    @Min(0)
    private int age;
    @Max(300)
    @Min(0)
    private int height;
    @Max(400)
    @Min(0)
    private int weight;
    private boolean isAlive;

    public UserAnnotations() {
    }

    public UserAnnotations(int age, int height, int weight, boolean isAlive) {
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.isAlive = isAlive;
    }

    public UserAnnotations(String name, int height, int weight, boolean isAlive) {
        this.name = name;
        this.height = height;
        this.weight = weight;
        this.isAlive = isAlive;
    }

    public UserAnnotations(String name, int age, int height, int weight, boolean isAlive) {
        this.name = name;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.isAlive = isAlive;
    }

    public UserAnnotations(String name, int age, int height, int weight) {
        this.name = name;
        this.age = age;
        this.height = height;
        this.weight = weight;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    @Override
    public String toString() {
        return "UserAnnotations{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                ", isAlive=" + isAlive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAnnotations that = (UserAnnotations) o;
        return age == that.age && height == that.height && weight == that.weight && isAlive == that.isAlive && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, height, weight, isAlive);
    }
}
