import abstract_factory.rpg.BoneSmither;
import abstract_factory.rpg.SteelSmither;
import abstract_factory.rpg.Weapon;

public class MainPatterns {
    public static void main(String[] args) {
        SteelSmither ironSmither = new SteelSmither();
        Weapon weapon1 = ironSmither.forgeSword("кладенец");
        Weapon weapon2 = ironSmither.forgeDagger("sting");
        Weapon weapon3 = ironSmither.forgeMace("дробилка");

        BoneSmither boneSmither = new BoneSmither();
        Weapon weapon4 = boneSmither.forgeSword("кость");
        Weapon weapon5 = boneSmither.forgeDagger("позвоночник");
        Weapon weapon6 = boneSmither.forgeMace("зуб");

        weapon1.bluntHit(1);
        weapon1.chopHit(1);
        weapon1.thurst(1);

        weapon2.bluntHit(1);
        weapon2.chopHit(2);
        weapon2.thurst(1);

        weapon3.bluntHit(1);
        weapon3.chopHit(2);
        weapon3.thurst(1);

        weapon4.bluntHit(1);
        weapon4.chopHit(2);
        weapon4.thurst(1);

        weapon5.bluntHit(1);
        weapon5.chopHit(2);
        weapon5.thurst(1);

        weapon6.bluntHit(1);
        weapon6.chopHit(2);
        weapon6.thurst(1);
    }
}
