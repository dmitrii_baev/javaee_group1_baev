package abstract_factory.rpg;

public interface Weapon {
    Weapon beForged(String name,
                    String type,
                    String material,
                    float distance,
                    float damage,
                    float bluntKoefficient,
                    float chopKoefficient,
                    float thurstKoefficient,
                    float durability,
                    float enchantmentCapacity ,
                    int count,
                    int value,
                    boolean enchantment);
    float bluntHit(float distance);
    float chopHit(float distance);
    float thurst(float distance);
}
