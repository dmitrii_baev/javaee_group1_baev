package abstract_factory.rpg;

public interface Armor {
    String beForged(String parameters);
}
