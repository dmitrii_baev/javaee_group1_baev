package abstract_factory.rpg;

public interface Smither {
    Weapon forgeSword(String name);
    Weapon forgeMace(String name);
    Weapon forgeDagger(String name);
    Weapon forgeAxe(String name);
    Armor forgeArmor();
}
