package abstract_factory.rpg;

public class SteelSmither implements Smither {
    private static final String MATERIAL = "steel";
    private static final String DEFAULT_SWORD_NAME = "usual";
    private static final String DEFAULT_SWORD_TYPE = "sword";
    private static final String DEFAULT_SWORD_MATERIAL = MATERIAL;
    private static final float DEFAULT_SWORD_DISTANCE = 10.0F;
    private static final float DEFAULT_SWORD_DAMAGE = 10.0F;
    private static final float DEFAULT_SWORD_BLUNTKOEFFICIENT = 0.5F;
    private static final float DEFAULT_SWORD_CHOPKOEFFICIENT = 1.0F;
    private static final float DEFAULT_SWORD_THURSTKOEFFICIENT = 1.2F;
    private static final float DEFAULT_SWORD_DURABILITY = 1000.0F;
    private static final float DEFAULT_SWORD_ENCHANTMENTCAPACITY = 1000.0F;
    private static final int DEFAULT_SWORD_COUNT = 1;
    private static final int DEFAULT_SWORD_VALUE = 30;
    private static final boolean DEFAULT_SWORD_ENCHANTMENT = false;

    private static final String DEFAULT_DAGGER_NAME = "usual";
    private static final String DEFAULT_DAGGER_TYPE = "dagger";
    private static final String DEFAULT_DAGGER_MATERIAL = MATERIAL;
    private static final float DEFAULT_DAGGER_DISTANCE = 10.0F;
    private static final float DEFAULT_DAGGER_DAMAGE = 10.0F;
    private static final float DEFAULT_DAGGER_BLUNTKOEFFICIENT = 0.5F;
    private static final float DEFAULT_DAGGER_CHOPKOEFFICIENT = 1.0F;
    private static final float DEFAULT_DAGGER_THURSTKOEFFICIENT = 1.2F;
    private static final float DEFAULT_DAGGER_DURABILITY = 1000.0F;
    private static final float DEFAULT_DAGGER_ENCHANTMENTCAPACITY = 1000.0F;
    private static final int DEFAULT_DAGGER_COUNT = 1;
    private static final int DEFAULT_DAGGER_VALUE = 30;
    private static final boolean DEFAULT_DAGGER_ENCHANTMENT = false;

    private static final String DEFAULT_MACE_NAME = "usual";
    private static final String DEFAULT_MACE_TYPE = "mace";
    private static final String DEFAULT_MACE_MATERIAL = MATERIAL;
    private static final float DEFAULT_MACE_DISTANCE = 10.0F;
    private static final float DEFAULT_MACE_DAMAGE = 10.0F;
    private static final float DEFAULT_MACE_BLUNTKOEFFICIENT = 0.5F;
    private static final float DEFAULT_MACE_CHOPKOEFFICIENT = 1.0F;
    private static final float DEFAULT_MACE_THURSTKOEFFICIENT = 1.2F;
    private static final float DEFAULT_MACE_DURABILITY = 1000.0F;
    private static final float DEFAULT_MACE_ENCHANTMENTCAPACITY = 1000.0F;
    private static final int DEFAULT_MACE_COUNT = 1;
    private static final int DEFAULT_MACE_VALUE = 30;
    private static final boolean DEFAULT_MACE_ENCHANTMENT = false;

    private static final String DEFAULT_AXE_NAME = "usual";
    private static final String DEFAULT_AXE_TYPE = "axe";
    private static final String DEFAULT_AXE_MATERIAL = MATERIAL;
    private static final float DEFAULT_AXE_DISTANCE = 10.0F;
    private static final float DEFAULT_AXE_DAMAGE = 10.0F;
    private static final float DEFAULT_AXE_BLUNTKOEFFICIENT = 0.5F;
    private static final float DEFAULT_AXE_CHOPKOEFFICIENT = 1.0F;
    private static final float DEFAULT_AXE_THURSTKOEFFICIENT = 1.2F;
    private static final float DEFAULT_AXE_DURABILITY = 1000.0F;
    private static final float DEFAULT_AXE_ENCHANTMENTCAPACITY = 1000.0F;
    private static final int DEFAULT_AXE_COUNT = 1;
    private static final int DEFAULT_AXE_VALUE = 30;
    private static final boolean DEFAULT_AXE_ENCHANTMENT = false;

    public SteelSmither() {
    }

    @Override
    public Weapon forgeSword(String name) {
        return new SteelSword()
                .beForged(name,
                DEFAULT_SWORD_TYPE,
                DEFAULT_SWORD_MATERIAL,
                DEFAULT_SWORD_DISTANCE,
                DEFAULT_SWORD_DAMAGE,
                DEFAULT_SWORD_BLUNTKOEFFICIENT,
                DEFAULT_SWORD_CHOPKOEFFICIENT,
                DEFAULT_SWORD_THURSTKOEFFICIENT,
                DEFAULT_SWORD_DURABILITY,
                DEFAULT_SWORD_ENCHANTMENTCAPACITY,
                DEFAULT_SWORD_COUNT,
                DEFAULT_SWORD_VALUE,
                DEFAULT_SWORD_ENCHANTMENT);
    }

    @Override
    public Weapon forgeMace(String name) {
        return new SteelMace()
                .beForged(DEFAULT_MACE_NAME,
                DEFAULT_MACE_TYPE,
                DEFAULT_MACE_MATERIAL,
                DEFAULT_MACE_DISTANCE,
                DEFAULT_MACE_DAMAGE,
                DEFAULT_MACE_BLUNTKOEFFICIENT,
                DEFAULT_MACE_CHOPKOEFFICIENT,
                DEFAULT_MACE_THURSTKOEFFICIENT,
                DEFAULT_MACE_DURABILITY,
                DEFAULT_MACE_ENCHANTMENTCAPACITY,
                DEFAULT_MACE_COUNT,
                DEFAULT_MACE_VALUE,
                DEFAULT_MACE_ENCHANTMENT);
    }

    @Override
    public Weapon forgeDagger(String name) {
        return new SteelDagger()
                .beForged(name,
                DEFAULT_DAGGER_TYPE,
                DEFAULT_DAGGER_MATERIAL,
                DEFAULT_DAGGER_DISTANCE,
                DEFAULT_DAGGER_DAMAGE,
                DEFAULT_DAGGER_BLUNTKOEFFICIENT,
                DEFAULT_DAGGER_CHOPKOEFFICIENT,
                DEFAULT_DAGGER_THURSTKOEFFICIENT,
                DEFAULT_DAGGER_DURABILITY,
                DEFAULT_DAGGER_ENCHANTMENTCAPACITY,
                DEFAULT_DAGGER_COUNT,
                DEFAULT_DAGGER_VALUE,
                DEFAULT_DAGGER_ENCHANTMENT);
    }

    @Override
    public Weapon forgeAxe(String name) {
        return new SteelAxe()
                .beForged(DEFAULT_AXE_NAME,
                        DEFAULT_AXE_TYPE,
                        DEFAULT_AXE_MATERIAL,
                        DEFAULT_AXE_DISTANCE,
                        DEFAULT_AXE_DAMAGE,
                        DEFAULT_AXE_BLUNTKOEFFICIENT,
                        DEFAULT_AXE_CHOPKOEFFICIENT,
                        DEFAULT_AXE_THURSTKOEFFICIENT,
                        DEFAULT_AXE_DURABILITY,
                        DEFAULT_AXE_ENCHANTMENTCAPACITY,
                        DEFAULT_AXE_COUNT,
                        DEFAULT_AXE_VALUE,
                        DEFAULT_AXE_ENCHANTMENT);
    }

    @Override
    public Armor forgeArmor() {
        return null;
    }

    private static class SteelWeapon implements Weapon, Cloneable {
        private String name;
        private String type;
        private String material;
        private float distance;
        private float damage;
        private float bluntKoefficient;
        private float chopKoefficient;
        private float thurstKoefficient;
        private float durability;
        private float enchantmentCapacity;
        private int count;
        private int value;
        private boolean enchantment;

        //Реализация Builder через статический внутренний класс
        public static class Builder {
            //Обязательные параметры
            //Необязательные параметры со значениями по умолчанию
            public String name = DEFAULT_SWORD_NAME;
            public String type = DEFAULT_SWORD_TYPE;
            public String material = DEFAULT_SWORD_MATERIAL;
            public float distance = DEFAULT_SWORD_DISTANCE;
            public float damage = DEFAULT_SWORD_DAMAGE;
            public float bluntKoefficient = DEFAULT_SWORD_BLUNTKOEFFICIENT;
            public float chopKoefficient = DEFAULT_SWORD_CHOPKOEFFICIENT;
            public float thurstKoefficient = DEFAULT_SWORD_THURSTKOEFFICIENT;
            public float durability = DEFAULT_SWORD_DURABILITY;
            public float enchantmentCapacity = DEFAULT_SWORD_ENCHANTMENTCAPACITY;
            public int count = DEFAULT_SWORD_COUNT;
            public int value = DEFAULT_SWORD_VALUE;
            public boolean enchantment = DEFAULT_SWORD_ENCHANTMENT;

            //Методы с возвращающим типом Builder для необязательных параметров
            public Builder name(String name) {
                this.name = name;
                return this;
            }

            public Builder type(String type) {
                this.type = type;
                return this;
            }

            public Builder material(String material) {
                this.material = material;
                return this;
            }

            public Builder distance(float distance) {
                this.distance = distance;
                return this;
            }

            public Builder damage(float damage) {
                this.damage = damage;
                return this;
            }

            public Builder bluntKoefficient(float bluntKoefficient) {
                this.bluntKoefficient = bluntKoefficient;
                return this;
            }

            public Builder chopKoefficient(float chopKoefficient) {
                this.chopKoefficient = chopKoefficient;
                return this;
            }

            public Builder thurstKoefficient(float thurstKoefficient) {
                this.thurstKoefficient = thurstKoefficient;
                return this;
            }

            public Builder durability(float durability) {
                this.durability = durability;
                return this;
            }

            public Builder enchantmentCapacity(float enchantmentCapacity) {
                this.enchantmentCapacity = enchantmentCapacity;
                return this;
            }

            public Builder value(int value) {
                this.value = value;
                return this;
            }

            public Builder count(int count) {
                this.count = count;
                return this;
            }

            public Builder enchantment(boolean enchantment) {
                this.enchantment = enchantment;
                return this;
            }

            //Метод с возвращающим типом Good для генерации объекта
            public SteelWeapon build() {
                return new SteelWeapon(this);
            }
        }

        public Builder builder() {
            return new Builder();
        }

        private SteelWeapon(Builder builder) {
            name = builder.name;
            type = builder.type;
            material = builder.material;
            damage = builder.damage;
            distance = builder.distance;
            chopKoefficient = builder.chopKoefficient;
            thurstKoefficient = builder.thurstKoefficient;
            bluntKoefficient = builder.bluntKoefficient;
            enchantment = builder.enchantment;
            enchantmentCapacity = builder.enchantmentCapacity;
            durability = builder.durability;
            count = builder.count;
            value = builder.value;
        }

        public SteelWeapon() {
            this.beForged(DEFAULT_SWORD_NAME,
            DEFAULT_SWORD_TYPE,
            DEFAULT_SWORD_MATERIAL,
            DEFAULT_SWORD_DISTANCE,
            DEFAULT_SWORD_DAMAGE,
            DEFAULT_SWORD_BLUNTKOEFFICIENT,
            DEFAULT_SWORD_CHOPKOEFFICIENT,
            DEFAULT_SWORD_THURSTKOEFFICIENT,
            DEFAULT_SWORD_DURABILITY,
            DEFAULT_SWORD_ENCHANTMENTCAPACITY,
            DEFAULT_SWORD_COUNT,
            DEFAULT_SWORD_VALUE,
            DEFAULT_SWORD_ENCHANTMENT);
        }

        @Override
        public String toString() {
            return material + " " + type + " " + name;
        }

        @Override
        public Weapon beForged(String name,
                               String type,
                               String material,
                               float distance,
                               float damage,
                               float bluntKoefficient,
                               float chopKoefficient,
                               float thurstKoefficient,
                               float durability,
                               float enchantmentCapacity,
                               int count,
                               int value,
                               boolean enchantment) {
            return new Builder()
                    .name(name)
                    .type(type)
                    .material(material)
                    .distance(distance)
                    .damage(damage)
                    .bluntKoefficient(bluntKoefficient)
                    .chopKoefficient(chopKoefficient)
                    .thurstKoefficient(thurstKoefficient)
                    .durability(durability)
                    .enchantment(enchantment)
                    .count(count)
                    .value(value)
                    .enchantmentCapacity(enchantmentCapacity)
                    .build();
        }

        @Override
        public float bluntHit(float distance) {
            if ((distance <= this.distance) && (distance > 0)) {
                System.out.println("blunt hit with " + this + " damage - " + this.damage * this.bluntKoefficient);
                return this.damage * this.bluntKoefficient;
            } else {
                System.out.println("too far to hit with " + this);
                return 0;
            }
        }

        @Override
        public float chopHit(float distance) {
            if ((distance <= this.distance) && (distance > 0)) {
                System.out.println("chop hit with " + this + " damage - " + this.damage * this.chopKoefficient);
                return this.damage * this.chopKoefficient;
            } else {
                System.out.println("too far to hit with " + this);
                return 0;
            }
        }

        @Override
        public float thurst(float distance) {
            if ((distance <= this.distance) && (distance > 0)) {
                System.out.println("thurst hit with " + this + " damage - " + this.damage * this.thurstKoefficient);
                return this.damage * this.thurstKoefficient;
            } else {
                System.out.println("too far to hit");
                return 0;
            }
        }

        @Override
        public SteelWeapon clone() {
            return new Builder().name(this.name)
                    .value(this.value)
                    .count(this.count)
                    .bluntKoefficient(this.bluntKoefficient)
                    .chopKoefficient(this.chopKoefficient)
                    .damage(this.damage)
                    .distance(this.distance)
                    .material(this.material)
                    .thurstKoefficient(this.thurstKoefficient)
                    .type(this.type)
                    .name(this.name)
                    .durability(this.durability)
                    .enchantment(this.enchantment)
                    .enchantmentCapacity(this.enchantmentCapacity)
                    .build();
        }
    }

    private static class SteelSword extends SteelWeapon{
        public SteelSword() {
            this.beForged(DEFAULT_SWORD_NAME,
                    DEFAULT_SWORD_TYPE,
                    DEFAULT_SWORD_MATERIAL,
                    DEFAULT_SWORD_DISTANCE,
                    DEFAULT_SWORD_DAMAGE,
                    DEFAULT_SWORD_BLUNTKOEFFICIENT,
                    DEFAULT_SWORD_CHOPKOEFFICIENT,
                    DEFAULT_SWORD_THURSTKOEFFICIENT,
                    DEFAULT_SWORD_DURABILITY,
                    DEFAULT_SWORD_ENCHANTMENTCAPACITY,
                    DEFAULT_SWORD_COUNT,
                    DEFAULT_SWORD_VALUE,
                    DEFAULT_SWORD_ENCHANTMENT);

        }
    }

    private static class SteelDagger extends SteelWeapon{
        public SteelDagger() {
            this.beForged(DEFAULT_DAGGER_NAME,
                    DEFAULT_DAGGER_TYPE,
                    DEFAULT_DAGGER_MATERIAL,
                    DEFAULT_DAGGER_DISTANCE,
                    DEFAULT_DAGGER_DAMAGE,
                    DEFAULT_DAGGER_BLUNTKOEFFICIENT,
                    DEFAULT_DAGGER_CHOPKOEFFICIENT,
                    DEFAULT_DAGGER_THURSTKOEFFICIENT,
                    DEFAULT_DAGGER_DURABILITY,
                    DEFAULT_DAGGER_ENCHANTMENTCAPACITY,
                    DEFAULT_DAGGER_COUNT,
                    DEFAULT_DAGGER_VALUE,
                    DEFAULT_DAGGER_ENCHANTMENT);
        }
    }

    private static class SteelMace extends SteelWeapon{
        public SteelMace() {
            this.beForged(DEFAULT_MACE_NAME,
                    DEFAULT_MACE_TYPE,
                    DEFAULT_MACE_MATERIAL,
                    DEFAULT_MACE_DISTANCE,
                    DEFAULT_MACE_DAMAGE,
                    DEFAULT_MACE_BLUNTKOEFFICIENT,
                    DEFAULT_MACE_CHOPKOEFFICIENT,
                    DEFAULT_MACE_THURSTKOEFFICIENT,
                    DEFAULT_MACE_DURABILITY,
                    DEFAULT_MACE_ENCHANTMENTCAPACITY,
                    DEFAULT_MACE_COUNT,
                    DEFAULT_MACE_VALUE,
                    DEFAULT_MACE_ENCHANTMENT);
        }
    }

    private static class SteelAxe extends SteelWeapon{
        public SteelAxe() {
            this.beForged(DEFAULT_AXE_NAME,
                    DEFAULT_AXE_TYPE,
                    DEFAULT_AXE_MATERIAL,
                    DEFAULT_AXE_DISTANCE,
                    DEFAULT_AXE_DAMAGE,
                    DEFAULT_AXE_BLUNTKOEFFICIENT,
                    DEFAULT_AXE_CHOPKOEFFICIENT,
                    DEFAULT_AXE_THURSTKOEFFICIENT,
                    DEFAULT_AXE_DURABILITY,
                    DEFAULT_AXE_ENCHANTMENTCAPACITY,
                    DEFAULT_AXE_COUNT,
                    DEFAULT_AXE_VALUE,
                    DEFAULT_AXE_ENCHANTMENT);
        }
    }
}