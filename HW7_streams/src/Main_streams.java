import service.Car;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main_streams {
    public static void main(String[] args) {
        //Car mapper
        Function<String, Car> carMapper = line -> {
            String[] parsedLine = line.split("\\|");
            return new Car(parsedLine[0],
                    parsedLine[1],
                    parsedLine[2],
                    Integer.parseInt(parsedLine[3]),
                    Integer.parseInt(parsedLine[4]));
        };
        String path = new String("C:\\JAVA\\javaee_group1_baev_prepared\\HW7_streams\\src\\cars.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader(path));){
            System.out.println("1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.");
            String colorFilter1 = new String("black");
            int distanceFilter1 = 0;
            List<Car> cars1 = reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getColor().equals(colorFilter1) || car.getDistance() == distanceFilter1)
                    .collect(Collectors.toList());
            for (int i = 0; i < cars1.size(); i++) System.out.println(cars1.get(i).getNumber());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(path));){
            System.out.println("2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.");
            int minPrice = 700;
            int maxPrice = 800;
            List<Car> cars2 = reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getPrice() > minPrice && car.getPrice() < maxPrice)
                    .distinct()
                    .collect(Collectors.toList());
            System.out.println(cars2.size());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(path));){
            System.out.println("3) Вывести цвет автомобиля с минимальной стоимостью.");
            Car car3 = reader
                    .lines()
                    .map(carMapper)
                    .min(Comparator.comparing(Car::getPrice))
                    .orElseThrow(IllegalStateException::new);
            System.out.println(car3.getColor());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(path));){
            System.out.println("4) Среднюю стоимость Camry");
            String lookingForModel = new String("camry");
            int averagePrice = 0;
            List<Car> cars4 = reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getModel().equals(lookingForModel))
                    .collect(Collectors.toList());
            for (int i = 0; i < cars4.size(); i++) averagePrice += cars4.get(i).getPrice();
            System.out.println(Long.valueOf(averagePrice/cars4.size()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(path));){
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
