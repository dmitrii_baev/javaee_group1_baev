package service;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;

public class Car implements Comparable<Car>  {
    private String number;
    private String model;
    private String color;
    private int distance;
    private int price;

    public Car(String number, String model, String color, int distance, int price){
        this.number = number;
        this.model = model;
        this.color = color;
        this.distance = distance;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return number + '|' +  model + '|'  + color + '|' + distance + '|' + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return distance == car.distance && price == car.price && Objects.equals(number, car.number) && Objects.equals(model, car.model) && Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, distance, price);
    }

    @Override
    public int compareTo(Car that) {
        return Long.compare(this.price, that.price);
    }
}
