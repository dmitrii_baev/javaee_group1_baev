package service;

import java.util.Comparator;

class CarComparator implements Comparator<Car> {

    public int compare(Car a, Car b){

        return Long.valueOf(a.getPrice()).compareTo(Long.valueOf(b.getPrice()));
    }
}

