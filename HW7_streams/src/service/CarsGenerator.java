package service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Random;
import service.Car;

class CarGenerator {
    public static void main(String[] args) {
        String fileName = new String();
        fileName = "C:\\JAVA\\javaee_group1_baev_prepared\\HW7_streams\\src\\cars.txt";
        String models[] = new String[10];
        String colors[] = new String[10];
        String numberLetters = new String("ETOPKHACBM");
        String numberDigits = new String("1234567890");
        int maxPrice = 1000;
        int minPrice = 300;
        models[0] = "camry";
        models[1] = "jeep";
        models[2] = "corolla";
        models[3] = "z250";
        models[4] = "skyline";
        models[5] = "camaro";
        models[6] = "impala";
        models[7] = "mustang";
        models[8] = "panamera";
        models[9] = "911s";
        colors[0] = "red";
        colors[1] = "black";
        colors[2] = "white";
        colors[3] = "yellow";
        colors[4] = "orange";
        colors[5] = "gray";
        colors[6] = "blue";
        colors[7] = "violett";
        colors[8] = "marine";
        colors[9] = "green";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false));){
            Random random = new Random();
            Car[] carGenerator = new Car[100];
            for (int i = 0; i < 100; i++){
                carGenerator[i] = new Car( Character.toString(numberLetters.charAt(random.nextInt(numberLetters.length() - 1))) + Character.toString(numberDigits.charAt(random.nextInt(numberDigits.length() - 1))) + Character.toString(numberDigits.charAt(random.nextInt(numberDigits.length() - 1))) + Character.toString(numberDigits.charAt(random.nextInt(numberDigits.length() - 1))) + Character.toString(numberLetters.charAt(random.nextInt(numberLetters.length() - 1)))+ Character.toString(numberLetters.charAt(random.nextInt(numberLetters.length() - 1))) + Character.toString(numberDigits.charAt(random.nextInt(numberDigits.length() - 1))) + Character.toString(numberDigits.charAt(random.nextInt(numberDigits.length() - 1))) + Character.toString(numberDigits.charAt(random.nextInt(numberDigits.length() - 1))),
                        models[random.nextInt(models.length)],
                        colors[random.nextInt(colors.length)],
                        random.nextInt(10000),
                        minPrice + random.nextInt(maxPrice-minPrice));
                writer.write(carGenerator[i].toString());
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
