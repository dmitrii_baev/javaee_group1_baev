import java.io.*;
import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.concurrent.Callable;

public class Service{
    public static void downloading(String downloadingLink, String downloadingPath) throws IOException {
        URL url1 = new URL(downloadingLink);
        URLConnection connection1 = url1.openConnection();
        InputStream inputstream1 = connection1.getInputStream();
        Files.copy(inputstream1, new File(downloadingPath).toPath());
    }

    public static Callable<String> downloadingThread(String downloadingLink, String downloadingPath){
        Callable download1 = (Callable) () -> {
            downloading(downloadingLink, downloadingPath);
            return "скачали файл" + downloadingLink + " в " + downloadingPath;
        };
        return download1;
    }
}
