import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.stream.Stream;

public class Main_threads {
    public static void main(String[] args) throws IOException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Service downloadService = new Service();
        String downloadingPath = new String("C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture1.mp4");
        String downloadingLink = new String("https://r4---sn-5hnedn7l.googlevideo.com/videoplayback?expire=1632442187&ei=68JMYcnwAuq8xN8P0NSr4AY&ip=185.235.43.191&id=o-ACcIShKPJMnFAj8ytuAyJZO_qgd8kj4VCsieClaV-5iS&itag=22&source=youtube&requiressl=yes&mh=oj&mm=31%2C26&mn=sn-5hnedn7l%2Csn-4g5lznez&ms=au%2Conr&mv=m&mvi=4&pl=24&initcwndbps=523750&vprv=1&mime=video%2Fmp4&ns=4fvdJyIkRGBaJchPxMSgTIEG&cnr=14&ratebypass=yes&dur=3496.948&lmt=1631467089490902&mt=1632420298&fvip=4&fexp=24001373%2C24007246&c=WEB&txp=6211224&n=hstcWzeXwuhV0qpM5&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Ccnr%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIgWma-UDuxAtBG2WxFFQOWRImI2UXTvwS-d1Hxj8XAafgCIQDo7_XW_PUKiCWhEJxJFyBj32l9Rv1cqEG6aFi7QMAQsg%3D%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRQIgekF7h-gRTj6_N3dL-qQBDJtGOPPaVxo6XxrFNA3ZcbQCIQCHNXVGE3QeMYiEGJfo0DAxUDNcJyKy0iIzgjcVNL6k-g%3D%3D&title=%D0%97%D0%B0%D0%BD%D1%8F%D1%82%D0%B8%D0%B5%206.%20%D0%A7%D0%B0%D1%81%D1%82%D1%8C%201.%20%D0%9F%D0%B0%D0%BA%D0%B5%D1%82%20Java.io%20%D0%B8%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%20%D1%81%20%D1%80%D0%B5%D1%81%D1%83%D1%80%D1%81%D0%B0%D0%BC%D0%B8");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        Future<String> f1 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture2.mp4";
        Future<String> f2 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture3.mp4";
        Future<String> f3 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture4.mp4";
        Future<String> f4 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture5.mp4";
        Future<String> f5 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture6.mp4";
        Future<String> f6 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture7.mp4";
        Future<String> f7 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture8.mp4";
        Future<String> f8 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture9.mp4";
        Future<String> f9 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));
        downloadingPath = "C:\\JAVA\\javaee_group1_baev_prepared\\HW8_threads\\Downloaded\\picture10.mp4";
        Future<String> f10 = executorService.submit(Service.downloadingThread(downloadingLink, downloadingPath));

        Future<?>[] tasks = {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10};


        while (true) {
            int count = 0;
            for (Future<?> future : tasks) {
                if (future.isDone()) {
                    count++;
                }
            }

            if (count == tasks.length) {
                for (Future<?> task : tasks) {
                    try {
                        System.out.println(task.get());
                    } catch (InterruptedException | ExecutionException e) {
                        throw new IllegalArgumentException();
                    }
                }
                return;
            }
            executorService.shutdown();
        }
//        Stream.generate(() -> {
//            Callable<String> download = () -> {
//                URL url2 = new URL(downloadingLink);
//                URLConnection connection2 = url2.openConnection();
//                InputStream inputstream1 = connection2.getInputStream();
//                Files.copy(inputstream1, new File(downloadingPath10).toPath());
//                return "скачали файл" + downloadingLink + " в "+ downloadingPath10;
//            };
//            return download;
//        });
    }
}
