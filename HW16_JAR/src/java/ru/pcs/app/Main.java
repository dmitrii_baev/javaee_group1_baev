package java.ru.pcs.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.ru.pcs.config.Config;
import java.util.List;

import static sun.tools.jar.CommandLine.parse;

@Parameters(separators = "=")
class Main {

	@Parameter(names = {"--files"})
	private List<String> files;

	public static void main(String args[]) {
		Config config = new Config();
		System.out.println(config.getThreadsCount());
		Main main = new Main();

		JCommander jcomander = new JCommander();
		jcomander.addObject(main);
		jcomander.parse(args);

		System.out.println(main.files);
	}
}