package service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class RepositoryJDBCImpl implements ProductsRepository{
    //language=SQL
    private static final String SQL_SELECT_ALL = "select id, name from hw15.\"Product\" order by id limit ? offset ?;";//"select id, name from hw15.Products order by id limit ? offset ?;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select id, name from hw15.\"Product\" where id = ?";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from hw15.\"Product\" where id = ?";

    //language=SQL
    private static final String SQL_DELETE_BY_NAME = "delete from hw15.\"Product\" where name = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into hw15.\"Product\"(id, name) values (?, ?)";

    //language=SQL
    private static final String SQL_UPDATE = "update hw15.\"Product\" set id = ?, name = ? where id = ?;";

    private final DataSource dataSource;

    private static final Function<ResultSet, Product> productMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            return new Product(id, name);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public RepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Product> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)){
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(productMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Product> findAll(int page, int size) {
        List <Product> products = new ArrayList<Product>();
        try (Connection connection = this.dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL);){
            statement.setInt(1, size);
            statement.setInt(2, page*size);
            try (ResultSet resultSet = statement.executeQuery()){
                products.add(productMapper.apply(resultSet));
            }
            return products;
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, product.getId());
            statement.setString(2, product.getName());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert product");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                product.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't get id");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        try(Connection connection = this.dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);){
            statement.setLong(1, product.getId());
            statement.setString(2, product.getName());
            statement.setLong(3, product.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                System.out.println("Can't update product");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Product product) {
        try (Connection connection = this.dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_NAME);){
            statement.setString(1, product.getName());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                System.out.println("Can't delete product");
            }
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (Connection connection = this.dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID);){
            statement.setLong(1, id);

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                System.out.println("Can't delete product");
            }
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }
}
