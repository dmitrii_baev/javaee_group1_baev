import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import hickariJDBC.OnePerQueryDataSource;
import service.Product;
import service.RepositoryJDBCImpl;

import java.util.Scanner;

public class Main_Hicari_Repository {
private static final String DB_USER = "postgres";
private static final String DB_PASSWORD = "endwar777";
private static final String DB_URL = "jdbc:postgresql://localhost:5432/homeworks";
private static final String ORG_POSTGRESQL_DRIVER = "org.postgresql.Driver";


    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setUsername(DB_USER);
        config.setPassword(DB_PASSWORD);
        config.setDriverClassName(ORG_POSTGRESQL_DRIVER);
        config.setJdbcUrl(DB_URL);
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

//        OnePerQueryDataSource dataSource = new OnePerQueryDataSource(DB_USER, DB_PASSWORD, DB_URL);
        RepositoryJDBCImpl repository = new RepositoryJDBCImpl(dataSource);

        Scanner scanner = new Scanner(System.in);
        String pause1 = scanner.nextLine();

        Product newProduct1 = new Product(1l, "computer");
        Product newProduct2 = new Product(2l, "laptop");
        Product newProduct3 = new Product(3l, "mouse");
        Product newProduct2updated = new Product(2l, "gaming laptop");

        repository.save(newProduct1);
        String pause2 = scanner.nextLine();

        repository.save(newProduct2);
        String pause3 = scanner.nextLine();

        repository.save(newProduct3);
        String pause4 = scanner.nextLine();

        repository.update(newProduct2updated);
        String pause5 = scanner.nextLine();

        repository.delete(newProduct2updated);
        String pause6 = scanner.nextLine();

        repository.deleteById(1l);
        String pause7 = scanner.nextLine();
    }
}
