package ru.pcs.web.models;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString(exclude = "userEmail")
@Entity
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="original_filename")
    private String originalFileName;

    @Column(name="storage_filename")
    private String storageFileName;

    private Long size;
    private String mimeType;
    private String description;

    @ManyToOne
    @JoinColumn(name="account_id")
    private Account userEmail;
}
