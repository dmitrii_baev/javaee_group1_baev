package ru.pcs.web.repositories;

import ru.pcs.web.models.FileInfo;

import java.nio.file.Path;

public interface FileRepositoryJpa {

    void save(FileInfo file);

    Path findBy_originalFileName(String originalFileName);
}
