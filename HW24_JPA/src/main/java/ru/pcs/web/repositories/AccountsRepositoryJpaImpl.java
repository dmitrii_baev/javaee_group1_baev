package ru.pcs.web.repositories;

import ru.pcs.web.models.Account;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountsRepositoryJpaImpl implements AccountsRepositoryJpa {
    private final EntityManager entityManager;

    public AccountsRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Account account) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        Account accountCreated = Account.builder()
                .id(account.getId())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .password(account.getPassword())
                .email(account.getEmail())
                .build();

        entityManager.persist(accountCreated);

        transaction.commit();
    }

    @Override
    public List<Account> findAll() {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        List<Account> accounts = new ArrayList<>();
//        Query<Account> accountQuery = entityManager.createQuery("select account from Account account", Account.class);
        Query accountQuery = entityManager.createQuery("select account from Account account", Account.class);

        accounts = accountQuery.getResultList();
        transaction.commit();
        return accounts;
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Account list;
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> fileInfoRoot = criteriaQuery.from(Account.class);
        criteriaQuery.where(criteriaBuilder.like(fileInfoRoot.get("email"), '%' + email + '%'));
        Query query = entityManager.createQuery(criteriaQuery);
        list = (Account) query.getSingleResult();
        transaction.commit();
        return Optional.of(list);
    }

    @Override
    public List<Account> searchByEmail(String email) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        List<Account> accounts = new ArrayList<>();
//        Query<Account> accountQuery = entityManager.createQuery("select account from Account account where email = :param", Account.class);
        Query accountQuery = entityManager.createQuery("select account from Account account", Account.class);
//        accountQuery.setParameter("param", email);
        accounts = accountQuery.getResultList();
        transaction.commit();
        return accounts;
    }
}