package ru.pcs.web.repositories;

import ru.pcs.web.models.FileInfo;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileRepositoryJpaImpl implements FileRepositoryJpa {
    private final EntityManager entityManager;

    public FileRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(FileInfo file) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        FileInfo fileCreated = FileInfo.builder()
                .size(file.getSize())
                .mimeType(file.getMimeType())
                .description(file.getDescription())
                .originalFileName(file.getOriginalFileName())
//                .userEmail(file.getUserEmail())
                .storageFileName(file.getStorageFileName())
                .build();
        entityManager.persist(fileCreated);
        transaction.commit();
    }

    @Override
    public Path findBy_originalFileName(String originalFileName) {
        TypedQuery<FileInfo> query = entityManager.createQuery("select fileInfo from FileInfo fileInfo " +
                "where fileInfo.originalFileName = :originalFileName", FileInfo.class);
        query.setParameter("originalFileName", originalFileName);
        return Paths.get(query.getResultList().get(0).getOriginalFileName());
    }
}

