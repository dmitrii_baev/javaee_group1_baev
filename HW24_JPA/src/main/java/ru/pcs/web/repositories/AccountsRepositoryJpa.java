package ru.pcs.web.repositories;

import ru.pcs.web.models.Account;

import java.util.List;
import java.util.Optional;

public interface AccountsRepositoryJpa {
    void save(Account account);

    List<Account> findAll();

    Optional<Account> findByEmail(String email);

    List<Account> searchByEmail(String email);
}