package ru.pcs.web.app;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.web.models.Account;
import ru.pcs.web.models.FileInfo;
import ru.pcs.web.repositories.AccountsRepositoryJpa;
import ru.pcs.web.repositories.AccountsRepositoryJpaImpl;
import ru.pcs.web.repositories.FileRepositoryJpa;
import ru.pcs.web.repositories.FileRepositoryJpaImpl;

import javax.persistence.EntityManager;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        FileRepositoryJpa fileJpaRepository = new FileRepositoryJpaImpl(entityManager);

        AccountsRepositoryJpa accountsRepositoryJpa = new AccountsRepositoryJpaImpl(entityManager);
        Account dima = Account.builder()
                .firstName("ya")
                .lastName("snovaYa")
                .email("ya@yandex.ru")
                .password("qwerty700")
                .build();
        accountsRepositoryJpa.save(dima);

        FileInfo fileInfo = FileInfo.builder()
                .originalFileName("08220001.JPG")
                .description("Moscow")
                .userEmail(dima)
                .build();

        fileJpaRepository.save(fileInfo);


//        System.out.println(fileJpaRepository.findBy_originalFileName("08220001.JPG"));
    }
}
