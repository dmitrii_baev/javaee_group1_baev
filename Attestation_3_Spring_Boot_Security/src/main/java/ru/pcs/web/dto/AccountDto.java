package ru.pcs.web.dto;

import ru.pcs.web.models.Account;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String state;
    private String role;
    private String token;

    public static AccountDto from(Account account) {
        return AccountDto.builder()
                .id(account.getId())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .email(account.getEmail())
                .password(account.getPassword())
                .role(account.getRole().name())
                .state(account.getState().name())
                .token(account.getToken())
                .build();
    }

    public static List<AccountDto> from(List<Account> accounts) {
        return accounts.stream().map(AccountDto::from).collect(Collectors.toList());
    }

    public static AccountDto fromAndReduce(Account account) {
        return AccountDto.builder()
                .id(account.getId())
                .firstName(account.getFirstName())
                .lastName("SECURED")
                .email("SECURED")
                .password("SECURED")
                .role("SECURED")
                .state("SECURED")
                .token("SECURED")
                .build();
    }
    public static List<AccountDto> fromAndReduce(List<Account> accounts) {
        return accounts.stream().map(AccountDto::fromAndReduce).collect(Collectors.toList());
    }

}
