package ru.pcs.web.repositories;

import ru.pcs.web.models.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentsRepository extends JpaRepository<Student, Long> {
    Page<Student> findAllByIsDeletedIsNull(Pageable pageable);
}
