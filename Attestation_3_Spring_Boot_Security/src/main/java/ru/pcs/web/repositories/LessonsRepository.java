package ru.pcs.web.repositories;

import ru.pcs.web.models.Lesson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    Page<Lesson> findAllByIsDeletedIsNull(Pageable pageable);
}
