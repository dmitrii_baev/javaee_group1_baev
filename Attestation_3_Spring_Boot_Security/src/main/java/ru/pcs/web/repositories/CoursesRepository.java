package ru.pcs.web.repositories;

import ru.pcs.web.models.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursesRepository extends JpaRepository<Course, Long> {
    Page<Course> findAllByIsDeletedIsNull(Pageable pageable);

}
