package ru.pcs.web.services;

import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.LessonDto;

import java.util.List;

public interface CoursesService {

    List<CourseDto> getCourses(int page, int size);

    CourseDto addCourse(CourseDto course);

    CourseDto updateCourse(Long courseId, CourseDto course);

    void deleteCourse(Long courseId);

    List<LessonDto> addLessonToCourse(Long courseId, LessonDto lesson);

    List<LessonDto> deleteLessonFromCourse(Long courseId, LessonDto lesson);
}
