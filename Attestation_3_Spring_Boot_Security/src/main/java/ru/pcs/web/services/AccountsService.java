package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;

import java.util.List;

public interface AccountsService {
    List<AccountDto> getAccounts(int page, int size);

    List<AccountDto> getAccountsMinInfo(int page, int size);

    AccountDto addAccount(AccountDto accountDto);

    List<AccountDto> addAccountsList (List<AccountDto> accountDtoList);
}