package ru.pcs.web.services;

import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.models.Course;
import ru.pcs.web.models.Lesson;
import ru.pcs.web.repositories.CoursesRepository;
import ru.pcs.web.repositories.LessonsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CoursesServiceImpl implements CoursesService {
    private final CoursesRepository coursesRepository;
    private final LessonsRepository lessonsRepository;

    @Override
    public List<CourseDto> getCourses(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Course> result = coursesRepository.findAllByIsDeletedIsNull(request);
        return CourseDto.from( result.getContent());
    }

    @Override
    public CourseDto addCourse(CourseDto course) {

        Course newCourse = Course.builder()
                .id(course.getId())
                .title(course.getTitle())
                .build();
        coursesRepository.save(newCourse);
        return CourseDto.from(newCourse);
    }

    @Override
    public CourseDto updateCourse(Long courseId, CourseDto course) {
        Course existedCourse = coursesRepository.getById(courseId);
        existedCourse.setTitle(course.getTitle());
        coursesRepository.save(existedCourse);
        return CourseDto.from(existedCourse);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course existedCourse = coursesRepository.getById(courseId);
        existedCourse.setIsDeleted(true);
        coursesRepository.save(existedCourse);
    }

    @Override
    public List<LessonDto> addLessonToCourse(Long courseId, LessonDto lesson) {
        Course existedCourse = coursesRepository.getById(courseId);
        Lesson existedLesson = lessonsRepository.getById(lesson.getId());
        existedLesson.setCourse(existedCourse);
        lessonsRepository.save(existedLesson);
        return LessonDto.from(existedCourse.getLessons());
    }

    @Override
    public List<LessonDto> deleteLessonFromCourse(Long courseId, LessonDto lesson) {
        Course existedCourse = coursesRepository.getById(courseId);
        Lesson existedLesson = lessonsRepository.getById(lesson.getId());
        existedLesson.setCourse(null);
        lessonsRepository.save(existedLesson);
        return LessonDto.from(existedCourse.getLessons());
    }
}
