package ru.pcs.web.services;

import ru.pcs.web.dto.LessonDto;

import java.util.List;

public interface LessonsService {

    List<LessonDto> getLessons(int page, int size);

    LessonDto addLesson(LessonDto lesson);

    LessonDto updateLesson(Long lessonId, LessonDto lesson);

    void deleteLesson(Long lessonId);

}
