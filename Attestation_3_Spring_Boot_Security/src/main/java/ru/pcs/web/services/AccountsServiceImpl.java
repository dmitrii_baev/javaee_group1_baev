package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.models.Account;
import ru.pcs.web.repositories.AccountsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static ru.pcs.web.dto.AccountDto.from;
import static ru.pcs.web.dto.AccountDto.fromAndReduce;

@Service
@RequiredArgsConstructor
public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;
    private final PasswordEncoder passwordEncoder;


    @Override
    public List<AccountDto> getAccounts(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Account> result = accountsRepository.findAll(request);
        return from(result.getContent());
    }

    @Override
    public List<AccountDto> getAccountsMinInfo(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Account> result = accountsRepository.findAll(request);
        return fromAndReduce(result.getContent());
    }

    @Override
    public AccountDto addAccount(AccountDto accountDto) {
        Account newAccount = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .role(Account.Role.valueOf(accountDto.getRole()))
                .state(Account.State.CONFIRMED)
                .email(accountDto.getEmail())
                .password(passwordEncoder.encode(accountDto.getPassword()))
                .build();
        accountsRepository.save(newAccount);

        return from(newAccount);

    }

    @Override
    public List<AccountDto> addAccountsList(List<AccountDto> accountDtoList) {
        List<AccountDto> dtoList = new ArrayList<>();

        for(AccountDto accountDto : accountDtoList) {
            dtoList.add(addAccount(accountDto));
        }
        return dtoList;
    }
}
