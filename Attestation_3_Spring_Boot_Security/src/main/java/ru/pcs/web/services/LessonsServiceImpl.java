package ru.pcs.web.services;

import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.models.Lesson;
import ru.pcs.web.repositories.LessonsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static ru.pcs.web.dto.LessonDto.from;

@Service
@RequiredArgsConstructor
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;


    @Override
    public List<LessonDto> getLessons(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Lesson> result = lessonsRepository.findAllByIsDeletedIsNull(request);
        return from(result.getContent());
    }


    @Override
    public LessonDto addLesson(LessonDto lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .build();
        lessonsRepository.save(newLesson);
        return from(newLesson);
    }

    @Override
    public LessonDto updateLesson(Long lessonId, LessonDto lesson) {
        Lesson existedLesson = lessonsRepository.getById(lessonId);
        existedLesson.setName(lesson.getName());
        lessonsRepository.save(existedLesson);
        return from(existedLesson);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson existedLesson = lessonsRepository.getById(lessonId);
        existedLesson.setIsDeleted(true);
        lessonsRepository.save(existedLesson);
    }
}
