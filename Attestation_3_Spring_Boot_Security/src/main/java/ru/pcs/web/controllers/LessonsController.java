package ru.pcs.web.controllers;

import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.dto.LessonsResponse;
import ru.pcs.web.services.LessonsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/lessons")
@RequiredArgsConstructor
public class LessonsController {
    private final LessonsService lessonsService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<LessonsResponse> getLessons(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("someNewHeader", "some text which you want to receive"))
                .body(LessonsResponse.builder().data(lessonsService.getLessons(page, size)).build());



    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public LessonDto addLesson(@RequestBody LessonDto lesson) {
        return lessonsService.addLesson(lesson);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LessonDto updateLesson(@PathVariable("lesson-id") Long lessonId, @RequestBody LessonDto lesson) {
        return lessonsService.updateLesson(lessonId, lesson);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonsService.deleteLesson(lessonId);
    }

}
