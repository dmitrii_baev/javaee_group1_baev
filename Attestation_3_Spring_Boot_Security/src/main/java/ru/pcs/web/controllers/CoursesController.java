package ru.pcs.web.controllers;

import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.CoursesResponse;
import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.dto.LessonsResponse;
import ru.pcs.web.services.CoursesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/courses")
@RequiredArgsConstructor
public class CoursesController {
    private final CoursesService coursesService;


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CoursesResponse> getCourses(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(CoursesResponse.builder().data(coursesService.getCourses(page, size)).build());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public CourseDto addCourse(@RequestBody CourseDto course) {
        return coursesService.addCourse(course);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CourseDto updateCourse(@PathVariable("course-id") Long courseId, @RequestBody CourseDto course) {
        return coursesService.updateCourse(courseId, course);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteCourse(@PathVariable("course-id") Long courseId) {
        coursesService.deleteCourse(courseId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{course-id}/lessons")
    @ResponseStatus(HttpStatus.CREATED)
    public LessonsResponse addLessonToCourse(@PathVariable("course-id") Long courseId,
                                             @RequestBody LessonDto lesson) {
        return LessonsResponse.builder()
                .data(coursesService.addLessonToCourse(courseId, lesson))
                .build();
    }



}
