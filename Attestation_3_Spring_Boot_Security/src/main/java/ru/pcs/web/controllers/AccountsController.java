package ru.pcs.web.controllers;

import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.dto.AccountsResponse;
import ru.pcs.web.services.AccountsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class AccountsController {

    private final AccountsService accountsService;


    @GetMapping("/api/full-info/accounts")
    public ResponseEntity<AccountsResponse> getAccounts(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(AccountsResponse.builder().data(accountsService.getAccounts(page, size)).build());
    }

    @GetMapping("/api/accounts")
    public ResponseEntity<AccountsResponse> getAccountsMinInfo(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(AccountsResponse.builder().data(accountsService.getAccountsMinInfo(page, size)).build());
    }

    @PostMapping("/api/accounts")
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDto addAccount(@RequestBody AccountDto accountDto) {
        return accountsService.addAccount(accountDto);
    }

    @PostMapping("/api/accounts/list")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<AccountsResponse> addAccountsList(@RequestBody List<AccountDto> accountDtoList) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(AccountsResponse.builder().data(accountsService.addAccountsList(accountDtoList)).build());
    }
}
