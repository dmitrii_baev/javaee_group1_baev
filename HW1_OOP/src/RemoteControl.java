public interface RemoteControl {
    void On();
    void Off();
    void nextChannel();
    void prevChannel();
}
