public class Channel {
    private String name;
    private Program[] list;

    public Channel(String name) {
        this.name = name;
        this.list = new Program[1];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Program[] getList() {
        return list;
    }

    public Program getProgramm(int number) {
        return getList()[number];
    }

    public void setList(Program[] list) {
        this.list = list;
    }

    public void addProgramm(Program program){
        Program[] progList = new Program[getList().length + 1];
        Program[] tempList = getList();
        boolean isAdded = false;
        for (int i = 0; i < tempList.length; i++){
            if (tempList[i] == null && isAdded == false){
                progList[i] = program;
                isAdded = true;
            } else {
                progList[i] = tempList[i];
            }
        }
        if (isAdded == false) {
            progList[tempList.length] = program;
        }
        setList(progList);
    }

    /*public static void removeProgramm(Programm programm){
        Programm[] progList = new Programm[getList().length];
        for (int i = 0; i < getList().length; i++){
            if (getList()[i].getName() == programm.getName()) {
                progList[i] = getList()[i+1];
                i++;
            } else{
                progList[i] = getList()[i];
            }
        }
        setList(progList);
    }
     */

    @Override
    public String toString() {
        return "Channel - " + getName();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
