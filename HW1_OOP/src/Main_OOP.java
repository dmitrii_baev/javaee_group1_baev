public class Main_OOP {
    public static void main(String[] args) {
        TV tv = new TV("Samsung");
        Channel channel = new Channel("Первый");
        Program program1 = new Program("Давай поженимся");
        Program program2 = new Program("Малахов+");
        Program program3 = new Program("Пусть говорят");

        Channel channel1 = new Channel("ВТорой");
        Program program4 = new Program("Давай не поженимся");
        Program program5 = new Program("Малахов-");
        Program program6 = new Program("Пусть помолчат");

        RemoteTV controller = new RemoteTV(tv, "Пульт в виде самого младшего на диване");

        tv.addChannel(channel);
        channel.addProgramm(program1);
        channel.addProgramm(program2);
        channel.addProgramm(program3);
        tv.addChannel(channel1);
        channel1.addProgramm(program4);
        channel1.addProgramm(program5);
        channel1.addProgramm(program6);

        controller.On(); // случайную программу из Первого канала
        controller.nextChannel();
        controller.nextChannel();
        controller.prevChannel();
        controller.Off();

    }
}
