import java.util.Objects;
import java.util.Random;

public class RemoteTV implements RemoteControl {
    private String name;
    private TV tv;
    private int currentChan;

    Random randomiser = new Random();

    public RemoteTV(TV tv, String name) {
        this.tv = tv;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TV getTv() {
        return tv;
    }

    public void setTv(TV tv) {
        this.tv = tv;
    }

    public int getCurrentChan() {
        return currentChan;
    }

    public void setCurrentChan(int currentChan) {
        this.currentChan = currentChan;
    }

    @Override
    public void On() {
        System.out.println("Включили Телек " + this.tv.getName());
        currentChan = 0;
        System.out.println(this.tv.getChannel(currentChan).getProgramm(randomiser.nextInt(this.tv.getChannel(currentChan).getList().length - 1)).getName());
    }

    @Override
    public void Off() {
        System.out.println("Выключили Телек " + this.tv.getName());
        //return null;
    }

    @Override
    public void nextChannel() {
        int numberOfChan = this.tv.getList().length;
        if (currentChan >= (numberOfChan - 1)){
            currentChan = 0;
        } else {
            currentChan++;
        }
        System.out.println("Включен канал - " + currentChan);
        //return tv.getChannel(currentChan).getProgramm(randomiser.nextInt(tv.getChannel(currentChan).getList().length - 1));
    }

    @Override
    public void prevChannel() {
        int numberOfChan = this.tv.getList().length;
        if (currentChan <= 0){
            currentChan = numberOfChan - 1;
        } else {
            currentChan--;
        }
        System.out.println("Включен канал - " + currentChan);
        //return tv.getChannel(currentChan).getProgramm(randomiser.nextInt(tv.getChannel(currentChan).getList().length - 1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RemoteTV remoteTV = (RemoteTV) o;
        return Objects.equals(randomiser, remoteTV.randomiser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(randomiser);
    }
}
