public class TV {
    private static String name;
    private static Channel[] list;

    public TV(String name) {
        this.name = name;
        this.list = new Channel[1];
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        TV.name = name;
    }

    public static Channel[] getList() {
        return list;
    }

    public static Channel getChannel(int number) {
        return getList()[number];
    }

    public static void setList(Channel[] list) {
        TV.list = list;
    }

    public static void addChannel(Channel channel){
        Channel[] chanList = new Channel[getList().length + 1];
        Channel[] tempList = getList();
        boolean isAdded = false;
        for (int i = 0; i < tempList.length; i++){
            if (tempList[i] == null && isAdded == false){
                chanList[i] = channel;
                isAdded = true;
            } else {
                chanList[i] = tempList[i];
            }
        }
        if (isAdded == false) {
            chanList[tempList.length] = channel;
        }
        setList(chanList);
    }

    @Override
    public String toString() {
        return "TV - " + getName();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
