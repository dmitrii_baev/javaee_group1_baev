import Service.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main_Patterns2 {
    public static void main(String[] args) {
        MyFileReader newTry = new MyFileReader();
        MyBufferedFileReader myBuffer = new MyBufferedFileReader();
        MyStreamReader streamreader = new MyStreamReader();
//        another variant
//        newTry.setMyFileReaderStrategy(myBuffer);
        newTry.setMyFileReaderStrategy(streamreader);
        List<String> carsFromFile = newTry.readFromFile("C:\\JAVA\\javaee_group1_baev_prepared\\HW13_Patterns2\\src\\cars.txt");

//        watchers
        ListWatcher blackList = new ListWatcher(carsFromFile);
        ListWatcher theftAuto = new ListWatcher(carsFromFile);
        blackList.toWatchOverCars(carsFromFile, () ->{
            try(FileWriter writer = new FileWriter("C:\\JAVA\\javaee_group1_baev_prepared\\HW13_Patterns2\\src\\BlackListCars.txt");) {
                for (int i = 0; i < carsFromFile.size(); i++){
                    if (carsFromFile.get(i).split("\\|").length != 5) {
                        writer.write(carsFromFile.get(i) + "\n");
                        //writer.newLine();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

//        chain
        StringValidator validator = new StringValidator();
        NumberExtractor numberExtractor = new NumberExtractor();
        validator.setNextExtractor(numberExtractor);
        ModelExtractor modelExtractor = new ModelExtractor();
        numberExtractor.setNextExtractor(modelExtractor);
        ColourExtractor colourExtractor = new ColourExtractor();
        modelExtractor.setNextExtractor(colourExtractor);
        DistanceExtractor distanceExtractor = new DistanceExtractor();
        colourExtractor.setNextExtractor(distanceExtractor);
        PriceExtractor priceExtractor = new PriceExtractor();
        distanceExtractor.setNextExtractor(priceExtractor);
        theftAuto.toWatchOverCars(carsFromFile, () ->{
            List<String> theftCars = newTry.readFromFile("C:\\JAVA\\javaee_group1_baev_prepared\\HW13_Patterns2\\src\\theftCars.txt");
            for (int i = 0; i < carsFromFile.size(); i++){
                validator.extract(carsFromFile.get(i));
                if (theftCars.contains(carsFromFile.get(i))) System.out.println("Car " + carsFromFile.get(i).replace('|', ' ') + " is theft.");
            }
        });

        blackList.watch();
        theftAuto.watch();
        System.out.println(carsFromFile.toString());
    }
}
