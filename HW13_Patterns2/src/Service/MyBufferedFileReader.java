package Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyBufferedFileReader implements MyFileReaderStrategy{
    private List<String> stringsFromFile;

    public List<String> getStringsFromFile() {
        return stringsFromFile;
    }

    public void setStringsFromFile(List<String> stringsFromFile) {
        this.stringsFromFile = stringsFromFile;
    }

    public MyBufferedFileReader() {
        this.stringsFromFile = new ArrayList<String>();
    }

    @Override
    public List<String> readFromFile(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            Scanner scan = new Scanner(reader);
            while (scan.hasNextLine()) {
                stringsFromFile.add(scan.nextLine());
            }
            return stringsFromFile;
        }  catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
