package Service;

import java.util.ArrayList;
import java.util.List;

public class MyFileReader {
    private List<String> stringsFromFile;

    public List<String> getStringsFromFile() {
        return stringsFromFile;
    }

    public void setStringsFromFile(List<String> stringsFromFile) {
        this.stringsFromFile = stringsFromFile;
    }

    private MyFileReaderStrategy strategyOfReading;

    public MyFileReader() {
        this.stringsFromFile = new ArrayList<String>();
    }

    public List<String> readFromFile(String fieName){
        return strategyOfReading.readFromFile(fieName);
    }

    public void setMyFileReaderStrategy(MyFileReaderStrategy strategy){
        this.strategyOfReading = strategy;
    }
}
