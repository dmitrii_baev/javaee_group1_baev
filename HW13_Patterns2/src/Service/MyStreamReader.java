package Service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyStreamReader implements MyFileReaderStrategy{
    private List<String> stringsFromFile;

    public MyStreamReader() {
        this.stringsFromFile = new ArrayList<String>();
    }

    @Override
    public List<String> readFromFile(String fileName) {
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stringsFromFile = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringsFromFile;
    }
}
