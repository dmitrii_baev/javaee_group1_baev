package Service;

import java.util.List;

public class ListWatcher {
    private Watcher watcher;
    private List<String> listToCheck;

    public List<String> getListToCheck() {
        return listToCheck;
    }

    public ListWatcher(List<String> list){
        this.listToCheck = list;
    }
    public void toWatchOverCars(List<String> listToCheck, Watcher watcher) {
        this.watcher = watcher;
    }

    public void watch(){
        watcher.toWatchOverCars();
    }
}