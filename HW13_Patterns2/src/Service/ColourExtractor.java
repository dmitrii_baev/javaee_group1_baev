package Service;


import java.io.FileWriter;
import java.io.IOException;

public class ColourExtractor implements FileExtractror {
    protected FileExtractror next;

    @Override
    public void extract(String fileString) {
        try(FileWriter writer = new FileWriter("C:\\JAVA\\javaee_group1_baev_prepared\\HW13_Patterns2\\src\\Colours.txt", true)) {
            writer.write(fileString.split("\\|")[2] + "\n");
            nextExtractror(fileString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setNextExtractor(FileExtractror nextExtractor) {
        this.next = nextExtractor;
    }

    protected void nextExtractror(String fileString) {
        if (next != null) {
            next.extract(fileString);
        }
    }
}
