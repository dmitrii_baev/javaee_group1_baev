package Service;

import java.util.List;

public interface MyFileReaderStrategy {
    public List<String> readFromFile(String fileName);
}
