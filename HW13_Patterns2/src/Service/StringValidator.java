package Service;

public class StringValidator implements FileExtractror{
    protected FileExtractror next;

    @Override
    public void extract(String fileString) {
        if (fileString.split("\\|").length == 5) nextExtractror(fileString);
        else System.out.println("строка не делится на 5 частей");
    }

    @Override
    public void setNextExtractor(FileExtractror nextExtractor) {
        this.next = nextExtractor;
    }

    protected void nextExtractror(String fileString) {
        if (next != null) {
            next.extract(fileString);
        }
    }
}
