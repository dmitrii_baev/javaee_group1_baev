package Service;

interface FileExtractror {
    void extract(String fileString);
    void setNextExtractor(FileExtractror extractor);
}
