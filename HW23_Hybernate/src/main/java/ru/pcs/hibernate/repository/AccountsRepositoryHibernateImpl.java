package ru.pcs.hibernate.repository;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.pcs.hibernate.models.Account;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class AccountsRepositoryHibernateImpl implements AccountsRepository {

    private final Session session;
    private final EntityManager entityManager;



    public AccountsRepositoryHibernateImpl(SessionFactory sessionFactory) {
        this.session = sessionFactory.openSession();
        this.entityManager = this.session.getEntityManagerFactory().createEntityManager();

    }

    @Override
    public void save(Account account) {
        session.beginTransaction();

        Account accountCreated = Account.builder()
                .id(account.getId())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .password(account.getPassword())
                .email(account.getEmail())
                .build();

        session.merge(accountCreated);

        session.getTransaction().commit();
    }

    @Override
    public List<Account> findAll() {
        session.beginTransaction();
        List<Account> accounts = new ArrayList<>();
        Query<Account> accountQuery = session.createQuery("from Account account", Account.class);

        accounts = accountQuery.getResultList();
        session.getTransaction().commit();
        return accounts;
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        Account list;
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> fileInfoRoot = criteriaQuery.from(Account.class);
        criteriaQuery.where(criteriaBuilder.like(fileInfoRoot.get("email"), '%' + email + '%'));
        Query query = session.createQuery(criteriaQuery);
        list = (Account) query.getSingleResult();

        return Optional.of(list);
    }

    @Override
    public List<Account> searchByEmail(String email) {
        session.beginTransaction();
        List<Account> accounts = new ArrayList<>();
        Query<Account> accountQuery = session.createQuery("select account from Account account where email = :param", Account.class);
        accountQuery.setParameter("param", email);
        accounts = accountQuery.getResultList();
        session.getTransaction().commit();
        return accounts;
    }
}

