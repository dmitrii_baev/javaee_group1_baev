package ru.pcs.hibernate.models;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private long id;

    private String firstName;
    private String lastName;

    private String email;
    private String password;

}

