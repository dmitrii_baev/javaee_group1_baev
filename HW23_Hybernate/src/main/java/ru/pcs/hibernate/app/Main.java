package ru.pcs.hibernate.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.models.Account;
import ru.pcs.hibernate.repository.AccountsRepositoryHibernateImpl;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        AccountsRepositoryHibernateImpl repo = new AccountsRepositoryHibernateImpl(sessionFactory);
        Account acc1 = Account.builder()
                .email("test122345@")
                .password("pass")
                .lastName("tester5")
                .firstName("TesterLastname")
                .build();
        repo.save(acc1);
        System.out.println(repo.findByEmail("test122345@").toString());
        session.close();
        sessionFactory.close();

    }
}
