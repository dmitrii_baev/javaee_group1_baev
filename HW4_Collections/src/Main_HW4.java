import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main_HW4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите строку");

        String inputString = scanner.nextLine();

        Map<String, Integer> stringMap = new HashMap<>();

        String[] inputStringArray = inputString.split("");

        String[] allSymbols = new String[inputString.length()];

        int allSymbolsCounter = 0;

        for (int i = 0; i < inputString.length(); i++) {
            if (stringMap.get(inputStringArray[i]) == null){
                stringMap.put(inputStringArray[i], 1);
                allSymbols[allSymbolsCounter] = inputStringArray[i];
                allSymbolsCounter++;
            } else {
                stringMap.put(inputStringArray[i], stringMap.get(inputStringArray[i]) + 1);
            }
            //System.out.println(stringMap.get(inputStringArray[i]));
        }
        for (int i = 0; i < allSymbols.length; i++) {
            if (allSymbols[i] != null) System.out.println(allSymbols[i] + " - " + stringMap.get(allSymbols[i]));
            else break;
        }
            //System.out.println(stringMap);
    }
}