package service;

import java.util.List;
import java.util.Objects;

public class School {
    private int number;
    private String address;
    private List<User_Attestation> usersLists;

    public School(int number, String address) {
        this.number = number;
        this.address = address;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<User_Attestation> getUsersLists() {
        return usersLists;
    }

    public void setUsersLists(List<User_Attestation> usersLists) {
        this.usersLists = usersLists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        School school = (School) o;
        return number == school.number && Objects.equals(address, school.address) && Objects.equals(usersLists, school.usersLists);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, address, usersLists);
    }

    @Override
    public String toString() {
        return "School{" +
                "number=" + number +
                ", address='" + address + '\'' +
                ", usersLists=" + usersLists +
                '}';
    }
}
