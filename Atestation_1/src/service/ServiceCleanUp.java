package service;

import com.sun.deploy.cache.BaseLocalApplicationProperties;

import java.lang.reflect.Field;
import java.util.*;

public class ServiceCleanUp {;
    private static HashMap<Class, Object> valuesToDefault;

    public static void cleanup(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {
        if (object instanceof Map) {
            mapCleanup(object, fieldsToCleanup, fieldsToOutput);
        } else {
            notMapCleanup(object, fieldsToCleanup, fieldsToOutput);
        }
    }

    public static void mapCleanup(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput)
    {
        // Add all methods to cleanup or to output to the Collection
        Set<String> eachKey = new HashSet<String>();
        eachKey.addAll(fieldsToCleanup);
        eachKey.addAll(fieldsToOutput);

        Map map = (Map) object;

        for (String key : eachKey){
            if (map.get(key) == null)
                throw new IllegalArgumentException("No such key " + key + " in input map " + object.getClass());
        }
        String toOut = new String("Keys to Out: ");
        for (String fieldToOut : fieldsToOutput){
            toOut += fieldToOut + " " + map.get(fieldToOut).toString() + "; ";
        }
        System.out.println(toOut);

        for (String fieldToClean : fieldsToCleanup){
            cleanMapKey(map, fieldToClean);
        }
    }

    private static void cleanMapKey(Map map, Object key) {
        map.remove(key);
        map.put(key, null);
    }

    public static void notMapCleanup(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput){
        // Add all methods to cleanup or to output to the Collection
        Set<String> eachField = new HashSet<String>();
        eachField.addAll(fieldsToCleanup);
        eachField.addAll(fieldsToOutput);

        // Add all methods tof the object to the Collection
        Set<String> objectFieldsMap = new HashSet<String>();
        Class<Object> odjectClass = (Class<Object>) object.getClass();
        Field[] odjectFields = odjectClass.getDeclaredFields();
        for (Field field : odjectFields) {
            objectFieldsMap.add(field.getName());
        }
        //Check for possibility to clean or to route to output all the fields
        //Or throw Exception
        for (String field : eachField) {
            if (objectFieldsMap.contains(field)) {
//                System.out.println("field " + field + " exists in object " + object.getClass());
            } else throw new IllegalArgumentException("No such field " + field + " in object " + object.getClass());
        }
        String toOut = new String("Fields to Out: ");
        for (String fieldToOut : fieldsToOutput){
            try {
                Field fieldtoSOUT = object.getClass().getDeclaredField(fieldToOut);
                fieldtoSOUT.setAccessible(true);
                toOut += fieldToOut + " " + fieldtoSOUT.get(object).toString() + "; ";
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
        }
        System.out.println(toOut);
        checkObjectToCleanup(object, fieldsToCleanup);
//        Почему не работало вот так?
//        for (String fieldToCleanup : fieldsToCleanup){
//            try {
//                Field fieldToCleanupObj = object.getClass().getDeclaredField(fieldToCleanup);
//                fieldToCleanupObj.setAccessible(true);
//                if (valuesToDefault.get(fieldToCleanupObj.getType().getSimpleName()) != null)
//                    fieldToCleanupObj.set(fieldToCleanupObj.getType(), valuesToDefault.get(fieldToCleanupObj.getType().getSimpleName()));
////                else fieldToCleanupObj.set(Object, null);
//            } catch (NoSuchFieldException | IllegalAccessException e) {
//                throw new IllegalArgumentException(e);
//            }
//        }
    }

    public static void checkObjectToCleanup(Object object, Set<String> fieldsToCleanup){
        Field[] fields = object.getClass().getDeclaredFields();

        Map<String, String> types = new HashMap<>();
        types.put("byte", "0");
        types.put("short", "0");
        types.put("int","0");
        types.put("long", "0L");
        types.put("char", "\\u0000");
        types.put("double","0.0d");
        types.put("float","0.0f");


        for (Field field : fields) {
            if (fieldsToCleanup.contains(field.getName())) {
                if (types.containsKey(field.getType().getSimpleName())) {
                    field.setAccessible(true);
                    try {
                        field.set(object,0);
                    } catch (IllegalAccessException e) {
                        throw new IllegalArgumentException(e);
                    }
                } else {
                    field.setAccessible(true);
                    try {
                        field.set(object,null);
                    } catch (IllegalAccessException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
            }
        }
    }



    public ServiceCleanUp() {
        Map<Class, Object> defaultPrimitiveTypesValue = new HashMap<Class, Object>();
        int defaultInt = 0;
        float defaultFloat = 0;
        double defaultDouble = 0;
        boolean defautlBoolean = false;
        char defaultChar = 'o';
        byte dafultByte = 0;
        long defaultLong = 0;
        short defaultShort = 0;
        defaultPrimitiveTypesValue.put(int.class, defaultInt);
        defaultPrimitiveTypesValue.put(float.class, defaultFloat);
        defaultPrimitiveTypesValue.put(double.class, defaultDouble);
        defaultPrimitiveTypesValue.put(boolean.class, defautlBoolean);
        defaultPrimitiveTypesValue.put(char.class, defaultChar);
        defaultPrimitiveTypesValue.put(byte.class, dafultByte);
        defaultPrimitiveTypesValue.put(long.class, defaultLong);
        defaultPrimitiveTypesValue.put(short.class, defaultShort);

        this.valuesToDefault = (HashMap<Class, Object>) defaultPrimitiveTypesValue;
    }
}