package service;

import java.time.LocalDateTime;
import java.util.Objects;

public class User_Attestation {
    private static final int DEFAULT_STEPS_COUNT = 0;

    private String name;
    private String nativeTown;
    private LocalDateTime timeOfBirth;
    private int age;
    private int height;
    private int weight;
    private int stepsCount;
    private School usersSchool;


    public User_Attestation(String name, String nativeTown, LocalDateTime timeOfBirth, int age, int height, int weight, School school) {
        this.name = name;
        this.nativeTown = nativeTown;
        this.timeOfBirth = timeOfBirth;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.usersSchool = school;
        this.stepsCount = DEFAULT_STEPS_COUNT;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNativeTown() {
        return nativeTown;
    }

    public void setNativeTown(String nativeTown) {
        this.nativeTown = nativeTown;
    }

    public LocalDateTime getTimeOfBirth() {
        return timeOfBirth;
    }

    public void setTimeOfBirth(LocalDateTime timeOfBirth) {
        this.timeOfBirth = timeOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getStepsCount() {
        return stepsCount;
    }

    public void setStepsCount(int stepsCount) {
        this.stepsCount = stepsCount;
    }

    @Override
    public String toString() {
        return "User_Attestation{" +
                "name='" + name + '\'' +
                ", nativeTown='" + nativeTown + '\'' +
                ", timeOfBirth=" + timeOfBirth +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                ", stepsCount=" + stepsCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User_Attestation that = (User_Attestation) o;
        return age == that.age && height == that.height && weight == that.weight && stepsCount == that.stepsCount && Objects.equals(name, that.name) && Objects.equals(nativeTown, that.nativeTown) && Objects.equals(timeOfBirth, that.timeOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, nativeTown, timeOfBirth, age, height, weight, stepsCount);
    }
}
