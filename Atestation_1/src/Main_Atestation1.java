import service.School;
import service.ServiceCleanUp;
import service.User_Attestation;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static service.ServiceCleanUp.*;

public class Main_Atestation1 {
    public static void main(String[] args) {
        LocalDateTime time = LocalDateTime.now();
        School school1 = new School(1, "SOSH");
        User_Attestation user1 = new User_Attestation("Vanya", "Yaroslavl", time, 0, 52, 4, school1);
        ServiceCleanUp serviceCleaner = new ServiceCleanUp();
        Set<String> fieldsToCleanup = new HashSet<String>();
        Set<String> fieldsToOutput = new HashSet<String>();
        fieldsToCleanup.add("name");
//        fieldsToCleanup.add("usersSchool");
        fieldsToOutput.add("nativeTown");
        fieldsToOutput.add("height");
        fieldsToCleanup.add("age");
        System.out.println(user1);
        ServiceCleanUp.cleanup(user1, fieldsToCleanup, fieldsToOutput);
        System.out.println(user1);
        HashMap<String, String> testMap = new HashMap<String, String>();
        testMap.put("new", "old");
        testMap.put("new1", "old1");
        testMap.put("new2", "old2");
        Set<String> fieldsToCleanupMap = new HashSet<String>();
        Set<String> fieldsToOutputMap = new HashSet<String>();
        fieldsToOutputMap.add("new");
        fieldsToOutputMap.add("new2");
        fieldsToCleanupMap.add("new1");
        System.out.println(testMap);
        ServiceCleanUp.cleanup(testMap, fieldsToCleanupMap, fieldsToOutputMap);
        System.out.println(testMap);
    }
}
