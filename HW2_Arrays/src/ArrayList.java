public class ArrayList<E> implements List<E> {
    private int length;
    private E[] data;
    private int currentIndex;

    private static final double DEFAULT_SIZE_MULTIPLICATOR = 1.5;

    public ArrayList(int length) {
        if (length <= 0) {
            this.length = 1;
            this.data = (E[])new Object [1];
            this.currentIndex = 0;
        } else {
            this.length = length;
            this.data = (E[])new Object [length];
            this.currentIndex = 0;
        }
    }

    @Override
    public void add(E element) {
        checkSize();
        data[currentIndex] = element;
        currentIndex++;
    }

    @Override
    public E get(int index) {
        if (index < length) return data[index];
        else return null;
    }

    public int getLength() {
        return length;
    }


    private class ArrayListIterator<E> implements Iterator<E> {
        int cursor;

        public ArrayListIterator(int cursor) {
            this.cursor = cursor;
        }

        public ArrayListIterator() {
            this.cursor = cursor;
        }

        @Override
        public boolean hasNext() {
            if (cursor < (length - 1)) {
                return data[cursor + 1] != null;
            } else {
                System.out.println("Дошли до края массива");
                return false;
            }
        }

        @Override
        public E next() {
            if (hasNext()) {
                cursor++;
                return (E) data[cursor];
            } else {
                System.out.println("Курсор дошел до края массива");
                return null;
            }
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator(-1);
    }

    public E[] copyOfData(){
        Object copyOfArray[] = (E[]) new Object[this.length + 1];
        for (int i = 0; i < this.length; i++) copyOfArray[i] = data[i];
        return (E[]) copyOfArray;
    }

    public E[] cloneThisArray(E[] originalArray, int startindexOfOriginal, int startIndexOfClone, int cloneLength) {
        E[] cloneOfArray = (E[]) new Object[cloneLength];
        if (cloneOfArray.length >= (originalArray.length - startIndexOfClone)) {
            for (int i = startindexOfOriginal; i < (originalArray.length - startindexOfOriginal); i++) {
                cloneOfArray[startIndexOfClone + i - startindexOfOriginal] = originalArray[i];
            }
            return cloneOfArray;
        } else {
            System.out.println("несовпадение размеров массивов");
            return null;
        }
    }

    public void checkSize(){
        if (currentIndex >= length){
            E[] temp = copyOfData();
            length *= DEFAULT_SIZE_MULTIPLICATOR;
            data = cloneThisArray(temp, 0, 0, length);
        }
    }

}
