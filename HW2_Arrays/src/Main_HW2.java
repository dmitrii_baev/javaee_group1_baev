public class Main_HW2 {
    public static void main(String[] args) {
        ArrayList<String> myStrings = new ArrayList<String>(3);
        System.out.println(myStrings.getLength());
        myStrings.add("one");
        myStrings.add("two");
        myStrings.add("three");
        myStrings.add("four");
        myStrings.add("five");
        myStrings.add("six");
        System.out.println(myStrings.getLength());
        Iterator<String> stringIterator1 = myStrings.iterator();
        while (stringIterator1.hasNext()) System.out.println(stringIterator1.next());
    }
}
