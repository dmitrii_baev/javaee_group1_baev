package ru.pcs.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.pcs.config.Config;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

@Parameters(separators = "=")
class Main {

	@Parameter(names = {"--hikari-pool-size"})
	private List<Integer> poolSize;
	private static final String DB_USER = "postgres";
	private static final String DB_PASSWORD = "endwar777";
	private static final String DB_URL = "jdbc:postgresql://localhost:5432/homeworks";
	private static final String ORG_POSTGRESQL_DRIVER = "org.postgresql.Driver";
	private static final Logger logger = Logger.getLogger(Main.class);
	private List<Runnable> tasks = new ArrayList<>();

	public static void main(String[] args) {
		logger.info("Initialized thread pool...");
		ExecutorService executorService = Executors.newCachedThreadPool();
		Config config = new Config();
		Main main = new Main();
		logger.trace("Started JCommander");
		JCommander jCommander = new JCommander();
		jCommander.addObject(main);
		jCommander.parse(args);
		logger.debug("Jcommander finished his work");
		//читаем введенные при запуске строки и выводим их
		System.out.println("you've permited " + main.poolSize + " connections");
		logger.debug("Starting Hikari config");
		HikariConfig configHic = new HikariConfig();
		configHic.setUsername(DB_USER);
		configHic.setPassword(DB_PASSWORD);
		configHic.setDriverClassName(ORG_POSTGRESQL_DRIVER);
		configHic.setJdbcUrl(DB_URL);
		logger.info("Hikari started well");
		if (main.poolSize != null) {
			if (main.poolSize.get(0) != null) {
				configHic.setMaximumPoolSize(main.poolSize.get(0));//читаем введенный параметр
			} else configHic.setMaximumPoolSize(config.getThreadsCount());//читаем из файла указанного в Config параметр
		} else configHic.setMaximumPoolSize(config.getThreadsCount());//читаем из файла указанного в Config параметр
		HikariDataSource dataSource = new HikariDataSource(configHic);
		//тут можно уже отправлять запросы к БД
		executorService.shutdown();
		logger.info("Shutdown thread pool");
	}
}