package ru.pcs.web.services;

import ru.pcs.web.dto.SignInForm;
import ru.pcs.web.repositories.AccountsRepository;

public class SignInServiceImpl implements SignInService {

    private final AccountsRepository accountsRepository;

    public SignInServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public boolean doAuthenticate(SignInForm form) {
        return accountsRepository.findByEmail(form.getEmail())
                .map(account -> account.getPassword().equals(form.getPassword()))
                .orElse(false);

    }
}
