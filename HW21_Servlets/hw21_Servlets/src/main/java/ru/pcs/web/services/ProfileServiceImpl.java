package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.repositories.AccountsRepository;

import static ru.pcs.web.dto.AccountDto.from;

public class ProfileServiceImpl implements ProfileService {

    private final AccountsRepository accountsRepository;

    public ProfileServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public AccountDto getAccount(String email) {
        return from(accountsRepository.findByEmail(email).get());
    }
}
