package ru.pcs.web.services;

import ru.pcs.web.dto.FileDto;
import ru.pcs.web.models.Account;
import ru.pcs.web.models.FileInfo;
import ru.pcs.web.repositories.FileInfoRepository;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static ru.pcs.web.dto.FileDto.from;


public class FilesServiceImpl implements FilesService {

    private final FileInfoRepository fileInfoRepository;
    private String storagePath;

    public FilesServiceImpl(FileInfoRepository fileInfoRepository) {
        this.fileInfoRepository = fileInfoRepository;
    }

    @Override
    public void upload(FileDto form) {
        String fileName = form.getFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));

        FileInfo fileInfo = FileInfo.builder()
                .originalFileName(fileName)
                .storageFileName(UUID.randomUUID() + extension)
                .description(form.getDescription())
                .mimeType(form.getMimeType())
                .size(form.getSize())
                .accountId(form.getUserEmail())
                .build();

        fileInfoRepository.save(fileInfo);

        if (storagePath != null) {
            try {
                Files.copy(form.getFileStream(), Paths.get(storagePath + fileInfo.getStorageFileName()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    @Override
    public void setStoragePath(String path) {
        this.storagePath = path;
    }

    @Override
    public FileDto getFile(String storageFileName) {
        Optional<FileInfo> fileInfoOptional = fileInfoRepository.findByStorageFileName(storageFileName);
        if (fileInfoOptional.isPresent()) {
            FileDto file = from(fileInfoOptional.get());
            file.setFileName(storageFileName);
            return file;
        }
        return null;
    }

    @Override
    public void writeFile(FileDto file, OutputStream outputStream) {
        try {
            Files.copy(Paths.get(storagePath + "\\" + file.getFileName()), outputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<FileDto> searchByOriginalFileNameAndAccount(String originalFileName, Account account) {
        return from(fileInfoRepository.findByOriginalFileName(originalFileName, account));
//        return fileInfoRepository.searchAllFiles().stream()
//                .peek(System.out::println)
//                .map(FileDto::from)
//                .peek(System.out::println)
//                .collect(Collectors.toList());
    }
}
