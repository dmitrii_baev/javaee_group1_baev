package ru.pcs.web.servlets;

import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.repositories.FileInfoRepository;
import ru.pcs.web.repositories.FileInfoRepositoryJdbcImpl;
import ru.pcs.web.services.FilesService;
import ru.pcs.web.services.FilesServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;


@WebServlet("/files")
public class FilesServlet extends HttpServlet {
    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryJdbcImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository);
        this.filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String storageFileName = request.getParameter("fileName");
        FileDto file = filesService.getFile(storageFileName);
//        response.setContentType(file.getMimeType());
//        response.setContentLength((int) file.getSize());
//        response.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
//        filesService.writeFile(file, response.getOutputStream());
//        response.flushBuffer();

//        List<AccountDto> accounts = filesService.searchByOriginalFileName(request.getParameter("fileName"), request.getParameter());
//        request.setAttribute("accounts", accounts);
        request.getRequestDispatcher("jsp/searchFiles.jsp").forward(request, response);
    }
}
