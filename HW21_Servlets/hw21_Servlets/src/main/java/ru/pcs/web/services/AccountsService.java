package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;

import java.util.List;

public interface AccountsService {
    List<AccountDto> getAll();
    List<AccountDto> searchByEmail(String email);
}
