package ru.pcs.web.services;

import ru.pcs.web.dto.SignInForm;

public interface SignInService {
    boolean doAuthenticate(SignInForm form);
}
