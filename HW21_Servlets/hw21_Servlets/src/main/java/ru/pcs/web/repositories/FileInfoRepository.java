package ru.pcs.web.repositories;

import ru.pcs.web.models.Account;
import ru.pcs.web.models.FileInfo;

import java.util.List;
import java.util.Optional;


public interface FileInfoRepository {
    void save(FileInfo file);

    Optional<FileInfo> findByStorageFileName(String storageFileName);

    List<FileInfo> findByOriginalFileName(String originalFileName, Account account);

    List<FileInfo> searchAllFiles();
}
