<%@ page import="ru.pcs.web.dto.AccountDto" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<h1 style="color: ${color}">Profile</h1>
<br>

<table>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
    </tr>
    <tr>
        <td>${profile.firstName}</td>
        <td>${profile.lastName}</td>
        </tr>

</table>

<br>
    <form method="link" action="/logout">
    <input type="submit" value="Logout"/>
</form>
</body>
</html>
