<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search Files</title>
</head>

<script>
    function searchFiles(original_file_name) {
        let request = new XMLHttpRequest();
        request.open('GET', '/files/json?originalFileName=' + original_file_name);
        request.send();

        request.onload = function () {
            if (request.status !== 200) {
                alert("Ошибка!")
            } else {
                let html = '<tr>' +
                    '<th>Original File Name</th> + ' +
                    // '<th>Link</th>' +
                    '</tr>';


                let json = JSON.parse(request.response);
                console.log(json);

                for (let i = 0; i < json.length; i++) {
                    html += '<tr>'
                    const url = "/files/viewing?fileName=" + json[i]['fileName'];
                    html += '<td><a href="' + url + '">' + json[i]['fileName'] + ' </a></td>'
                    // html += '<td>' + json[i]['originalFileName'] + '</td>'
                    html += '<tr>'
                }
                document.getElementById('files_table').innerHTML = html;
            }
        }
    }
</script>

<body>
<label for="file_name">Search by filename</label>
<input id="file_name" type="text" placeholder="Enter filename"
       onkeyup="searchFiles(document.getElementById('file_name').value)">
<table id="files_table">

</table>
</body>
</html>
